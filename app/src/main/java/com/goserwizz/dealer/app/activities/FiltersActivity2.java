package com.goserwizz.dealer.app.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.goserwizz.dealer.app.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Kaptas on 4/1/2016.
 */
public class FiltersActivity2 extends AppCompatActivity {

    LinearLayout ModelLayout, modelone;
    TextView model_name;
    CheckBox check;
    String[] allmodels = new String[]{"Hyundai Centra", "Hyundai Accent", "Hyundai Fluid Verna", "Hyunai Santa Fe", "Hyundai i20 Active", "Hyundai Sonata", "Hyundai Eon"};
    List<String> allmodelList;
    LinearLayout datelay, modellay, servicelay, typelay, statuslay;
    LinearLayout calDatelay, AllModelay, ServiceCenterlay, ServiceTypelay, ServiceStatuslay;

    private ListView ModelList,serviceCenterList, serviceTypeList, servicestatusList ;
    private Filters[] models, serviceCenter, serviceType, serviceStatus ;
    private ArrayAdapter<Filters> ListmodelAdapter, listServiceCenterAdapter, listServiceTypeAdapter , listadapterStatusAdapter;
    private ArrayAdapter<FiltersSCenter> listserviceCenter;
    private FiltersSCenter[] service_Center;
    TextView date,model, center, type, status;
    ImageView filtercancel, resetdone;
    LinearLayout filter_header_layout, ResetLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filters_activity);

        datelay = (LinearLayout) findViewById(R.id.datelay);
        modellay = (LinearLayout) findViewById(R.id.modellay);
        servicelay = (LinearLayout) findViewById(R.id.scenterlay);
        typelay = (LinearLayout) findViewById(R.id.stypelay);
        statuslay = (LinearLayout) findViewById(R.id.sstatuslay);
        calDatelay = (LinearLayout) findViewById(R.id.CalendarDateLayout);
        ServiceCenterlay = (LinearLayout) findViewById(R.id.Service_Center_Layout);
        ServiceTypelay = (LinearLayout) findViewById(R.id.Service_Type_Layout);
        ServiceStatuslay = (LinearLayout) findViewById(R.id.Service_Status_Layout);
        ModelLayout = (LinearLayout) findViewById(R.id.ModelLayout);
        date = (TextView) findViewById(R.id.dateText);
        model = (TextView) findViewById(R.id.modelText);
        center = (TextView) findViewById(R.id.scenterText);
        type = (TextView) findViewById(R.id.stypeText);
        status = (TextView) findViewById(R.id.sstatusText);
        filtercancel = (ImageView) findViewById(R.id.filterCancel);
        resetdone = (ImageView) findViewById(R.id.FilterDoneImage);
        filter_header_layout = (LinearLayout) findViewById(R.id.filter_header_layout);
        ResetLayout = (LinearLayout) findViewById(R.id.ResetLayout);

        datelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.VISIBLE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#cc0000"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#FFFFFF"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#000000"));
            }
        });

        modellay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.VISIBLE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#cc0000"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#FFFFFF"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#000000"));

            }
        });

        servicelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.VISIBLE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#cc0000"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#FFFFFF"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#000000"));
            }
        });

        typelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.VISIBLE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#cc0000"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#FFFFFF"));
                status.setTextColor(Color.parseColor("#000000"));
            }
        });

        statuslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.VISIBLE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#cc0000"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });


        filtercancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterCancelDialog();
            }
            });

        filter_header_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterCancelDialog();
            }
        });

        resetdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //startActivity(new Intent(FiltersActivity2.this, FiltersActivity2.class));
                ListmodelAdapter.notifyDataSetChanged();

            }
        });

        ResetLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ListmodelAdapter.notifyDataSetChanged();
              //  startActivity(new Intent(FiltersActivity2.this, FiltersActivity2.class));
            }
        });



        /* Dynamically add all models Here to Models layout

          allmodelList = new ArrayList<String>(Arrays.asList(allmodels));
        for (int i = 0; i < allmodelList.size() - 1; i++) {
            LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            View modelview = inflater.inflate(R.layout.all_models, null);
            modelone = (LinearLayout) modelview.findViewById(R.id.model_one);
            model_name = (TextView) modelview.findViewById(R.id.model_text);
            check = (CheckBox) modelview.findViewById(R.id.model_checkBox);
            model_name.setText(allmodelList.get(i).toString());
            model_name.setTag(allmodelList.get(i));
            check.setTag(allmodelList.get(i));
            ModelLayout.addView(modelview);
        }
        */

        /*Find the ListView resource. for service center list starts  */
        ModelList = (ListView) findViewById( R.id.all_models_list );
        ModelList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick( AdapterView<?> parent, View item,
                                     int position, long id) {
                Filters filters = ListmodelAdapter.getItem( position );
                filters.toggleChecked();
                FiltersViewHolder viewHolder = (FiltersViewHolder) item.getTag();
                viewHolder.getCheckBox().setChecked( filters.isChecked() );
            }
        });
        // Create and populate contents.
        models = (Filters[]) getLastNonConfigurationInstance() ;
        if ( models == null ) {
            models = new Filters[] {
                    new Filters("Hyundai Centra"), new Filters("Hyundai Accent"),
                    new Filters("Hyundai Fluid Verna"), new Filters("Hyundai Santa Fe"),
                    new Filters("Hyundai i20 Active"), new Filters("Hyundai Sonata"),
                    new Filters("Hyundai Eon")
            };
        }
        ArrayList<Filters> modelList = new ArrayList<Filters>();
        modelList.addAll( Arrays.asList(models) );
        ListmodelAdapter = new FiltersArrayAdapter(this, modelList);
        ModelList.setAdapter( ListmodelAdapter );
   /*Find the ListView resource. for service center list ends  */


   /*Find the ListView resource. for service center list starts  */
        serviceCenterList = (ListView) findViewById( R.id.service_center_list );
        serviceCenterList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick( AdapterView<?> parent, View item,
                                     int position, long id) {
                FiltersSCenter filters = listserviceCenter.getItem( position );
                filters.toggleChecked();
                FiltersSCenterViewHolder viewHolder = (FiltersSCenterViewHolder) item.getTag();
                viewHolder.getCheckBox().setChecked( filters.isChecked() );
            }
        });
        // Create and populate contents.
        service_Center = (FiltersSCenter[]) getLastNonConfigurationInstance() ;
        if ( service_Center == null ) {
            service_Center = new FiltersSCenter[] {
                    new FiltersSCenter("Trident Hyundai-", "Jakksandra"), new FiltersSCenter("Trident Hyundai-", "Whitefields")
            };
        }
        ArrayList<FiltersSCenter> centerList = new ArrayList<FiltersSCenter>();
        centerList.addAll( Arrays.asList(service_Center) );
        listserviceCenter = new FiltersServiceCenterArrayAdapter(this, centerList);
        serviceCenterList.setAdapter( listserviceCenter );
   /*Find the ListView resource. for service center list ends  */

   /*Find the ListView resource. for service Type list  starts */
        serviceTypeList = (ListView) findViewById( R.id.service_type_list );
        serviceTypeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick( AdapterView<?> parent, View item,
                                     int position, long id) {
                Filters filter = listServiceTypeAdapter.getItem( position );
                filter.toggleChecked();
                FiltersViewHolder viewHolder = (FiltersViewHolder) item.getTag();
                viewHolder.getCheckBox().setChecked( filter.isChecked() );
            }
        });
        // Create and populate contents.
        serviceType = (Filters[]) getLastNonConfigurationInstance() ;
        if ( serviceType == null ) {
            serviceType = new Filters[] {
                    new Filters("First Free Service"), new Filters("Second Free Service"),
                    new Filters("Third Free Service"), new Filters("Paid Service"),
                    new Filters("Running Repairs"), new Filters("Body Shop")
            };
        }
        ArrayList<Filters> serviceTypelist = new ArrayList<Filters>();
        serviceTypelist.addAll( Arrays.asList(serviceType) );
        listServiceTypeAdapter = new FiltersArrayAdapter(this, serviceTypelist);
        serviceTypeList.setAdapter( listServiceTypeAdapter );
    /*Find the ListView resource. for service type list ends  */

    /*Find the ListView resource. for service Status list starts  */
        servicestatusList = (ListView) findViewById( R.id.service_status_list );
        servicestatusList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick( AdapterView<?> parent, View item,
                                     int position, long id) {
                Filters filter = listadapterStatusAdapter.getItem( position );
                filter.toggleChecked();
                FiltersViewHolder viewHolder = (FiltersViewHolder) item.getTag();
                viewHolder.getCheckBox().setChecked( filter.isChecked() );
            }
        });
        // Create and populate contents.
        serviceStatus = (Filters[]) getLastNonConfigurationInstance() ;
        if ( serviceStatus == null ) {
            serviceStatus = new Filters[] {
                    new Filters("Booked"), new Filters("Vehicle Received"),
                    new Filters("Work In Progress"), new Filters("Ready for Delivery"),
                    new Filters("Vehicle Delivered"), new Filters("Completed"),
                    new Filters("Cancelled")
            };
        }
        ArrayList<Filters> statusList = new ArrayList<Filters>();
        statusList.addAll( Arrays.asList(serviceStatus) );
        listadapterStatusAdapter = new FiltersArrayAdapter(this, statusList);
        servicestatusList.setAdapter( listadapterStatusAdapter );
     /*Find the ListView resource. for service status list ends   */



    }


    /** Holds planet data. */
private static class Filters {
    private String name = "" ;
    private boolean checked = false ;
    public Filters() {}
    public Filters( String name ) {
        this.name = name ;
    }
    public Filters( String name, boolean checked ) {
        this.name = name ;
        this.checked = checked ;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean isChecked() {
        return checked;
    }
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    public String toString() {
        return name ;
    }
    public void toggleChecked() {
        checked = !checked ;
    }
}

/** Holds child views for one row. */
private static class FiltersViewHolder {
    private CheckBox checkBox ;
    private TextView textView ;
    public FiltersViewHolder() {}
    public FiltersViewHolder( TextView textView, CheckBox checkBox ) {
        this.checkBox = checkBox ;
        this.textView = textView ;
    }
    public CheckBox getCheckBox() {
        return checkBox;
    }
    public void setCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }
    public TextView getTextView() {
        return textView;
    }
    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}

/** Custom adapter for displaying an array of Planet objects. */
private static class FiltersArrayAdapter extends ArrayAdapter<Filters> {

    private LayoutInflater inflater;

    public FiltersArrayAdapter(Context context, List<Filters> FiltersList) {
        super(context, R.layout.simplerow, R.id.rowTextView, FiltersList);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Filters planet = (Filters) this.getItem(position);
        CheckBox checkBox;
        TextView textView;

        // Create a new row view
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.simplerow, null);

            // Find the child views.
            textView = (TextView) convertView.findViewById(R.id.rowTextView);
            checkBox = (CheckBox) convertView.findViewById(R.id.CheckBox01);

            // Optimization: Tag the row with it's child views, so we don't have to
            // call findViewById() later when we reuse the row.
            convertView.setTag(new FiltersViewHolder(textView, checkBox));

            // If CheckBox is toggled, update the planet it is tagged with.
            checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    Filters filter = (Filters) cb.getTag();
                    filter.setChecked(cb.isChecked());
                }
            });
        }
        // Reuse existing row view
        else {
            // Because we use a ViewHolder, we avoid having to call findViewById().
            FiltersViewHolder viewHolder = (FiltersViewHolder) convertView.getTag();
            checkBox = viewHolder.getCheckBox();
            textView = viewHolder.getTextView();
        }

        // Tag the CheckBox with the Planet it is displaying, so that we can
        // access the planet in onClick() when the CheckBox is toggled.
        checkBox.setTag(planet);

        // Display planet data
        checkBox.setChecked(planet.isChecked());
        textView.setText(planet.getName());

        return convertView;
    }


}

    /*  For service Center List items Adapters functions */

    /** Holds planet data. */
    private static class FiltersSCenter {
        private String name = "" , place = "";
        private boolean checked = false ;
        public FiltersSCenter() {}
        public FiltersSCenter( String name, String place ) {
            this.name = name;
            this.place = place;
        }
        public FiltersSCenter( String name, String place, boolean checked ) {
            this.name = name ;
            this.place = place;
            this.checked = checked ;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }

        public String getPlace() {
            return place;
        }
        public void setPlace(String place) {
            this.place = place;
        }
        public boolean isChecked() {
            return checked;
        }
        public void setChecked(boolean checked) {
            this.checked = checked;
        }
        public String toString() {
            return name ;
        }
        public void toggleChecked() {
            checked = !checked ;
        }
    }

    /** Holds child views for one row. */
    private static class FiltersSCenterViewHolder {
        private CheckBox checkBox ;
        private TextView textView, place ;
        public FiltersSCenterViewHolder() {}
        public FiltersSCenterViewHolder( TextView textView, TextView place, CheckBox checkBox ) {
            this.checkBox = checkBox ;
            this.textView = textView ;
            this.place = place;
        }
        public CheckBox getCheckBox() {
            return checkBox;
        }
        public void setCheckBox(CheckBox checkBox) {
            this.checkBox = checkBox;
        }
        public TextView getTextView() {
            return textView;
        }
        public void setTextView(TextView textView) {
            this.textView = textView;
        }

        public TextView getplace() {
            return place;
        }
        public void setplace(TextView place) {
            this.place = place;
        }
    }

    /** Custom adapter for displaying an array of Planet objects. */
    private static class FiltersServiceCenterArrayAdapter extends ArrayAdapter<FiltersSCenter> {

        private LayoutInflater inflater;

        public FiltersServiceCenterArrayAdapter(Context context, List<FiltersSCenter> FiltersList) {
            super(context, R.layout.service_center_item, R.id.rowModel, FiltersList);
            // Cache the LayoutInflate to avoid asking for a new one each time.
            inflater = LayoutInflater.from(context);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Planet to display
            FiltersSCenter planet = (FiltersSCenter)this.getItem(position);

            // The child views in each row.
            CheckBox checkBox;
            TextView textView;
            TextView place;

            // Create a new row view
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.service_center_item, null);

                // Find the child views.
                textView = (TextView) convertView.findViewById(R.id.rowModel);
                place = (TextView) convertView.findViewById(R.id.rowPlace);
                checkBox = (CheckBox) convertView.findViewById(R.id.CheckBox01);

                // Optimization: Tag the row with it's child views, so we don't have to
                // call findViewById() later when we reuse the row.
                convertView.setTag(new FiltersSCenterViewHolder(textView, place, checkBox));

                // If CheckBox is toggled, update the planet it is tagged with.
                checkBox.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        FiltersSCenter filter = (FiltersSCenter) cb.getTag();
                        filter.setChecked(cb.isChecked());
                    }
                });
            }
            // Reuse existing row view
            else {
                // Because we use a ViewHolder, we avoid having to call findViewById().
                FiltersSCenterViewHolder viewHolder = (FiltersSCenterViewHolder) convertView.getTag();
                checkBox = viewHolder.getCheckBox();
                textView = viewHolder.getTextView();
                place = viewHolder.getplace();
            }

            // Tag the CheckBox with the Planet it is displaying, so that we can
            // access the planet in onClick() when the CheckBox is toggled.
            checkBox.setTag(planet);

            // Display planet data
            checkBox.setChecked(planet.isChecked());
            textView.setText(planet.getName());
            place.setText(planet.getPlace());

            return convertView;
        }

    }

    /*  Dialog fragment for Filter Cancel */

    public void FilterCancel(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Filters");
        alertDialogBuilder.setMessage("Continue without applying filters?");

        alertDialogBuilder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });

        alertDialogBuilder.setNegativeButton("APPLYFILTERS",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
             //   Toast.makeText(FiltersActivity2.this, "You clicked APPLYFILTERS", Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void filterr() {

        final Dialog filter = new Dialog(this);
        filter.setContentView(R.layout.custom_dialog);

        TextView applyfilter = (TextView)filter.findViewById(R.id.apply_filters);
        TextView continuefilter = (TextView)filter.findViewById(R.id.continue_filters);
        filter.show();

        applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                filter.dismiss();
            }
        });

        continuefilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter.dismiss();

            }
        });

    }

    public void FilterCancelDialog() {

        final Dialog filterDialog = new Dialog(this);
        filterDialog.setContentView(R.layout.custom_dialog);
      //  filterDialog.setTitle("Custom Dialog Box");

        TextView applyfilter = (TextView)filterDialog.findViewById(R.id.apply_filters);
        TextView continuefilter = (TextView)filterDialog.findViewById(R.id.continue_filters);

        applyfilter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                filterDialog.dismiss();
              //  Toast.makeText(FiltersActivity2.this, "You clicked APPLYFILTERS", Toast.LENGTH_LONG).show();
            }
        });
        continuefilter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                filterDialog.dismiss();

                startActivity(new Intent(FiltersActivity2.this, HomeActivity.class));
            }
        });
        filterDialog.show();


    }

}
