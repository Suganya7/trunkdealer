package com.goserwizz.dealer.app.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goserwizz.dealer.app.R;

/**
 * Created by Kaptas on 3/15/2016.
 */
public class Feedback extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.feedback, container, false);

        return v;

    }
}
