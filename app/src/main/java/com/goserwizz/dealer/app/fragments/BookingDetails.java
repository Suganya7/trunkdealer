package com.goserwizz.dealer.app.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.dealer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.Connectivity;
import com.goserwizz.dealer.app.Util.NetworkAvailable;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.adapters.CustomListAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/31/2016.
 */
public class BookingDetails extends Fragment {

    ListView listview;
    TextView bookingService_count;

    public static String[] Brand ={};
    public static String[] RegNum ={};
    public static String[] Datetime ={};
    public static String[] serviceCenter ={};
    public static String[] serviceID ={};
    public static String[] statusDetails ={};

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url;
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_GDID = "gd_id";
    private static final String TAG_DID = "did";
    private static final String TAG_ORDER = "order";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";

    String status, resultmsg, gd_id, emptyData, bookstatus;
    String serviceType, dealerCountry, mob, dealerCity, cmts, dealerAddr, advance, dealerLocation, slotTime, paymode, dealerCompName,
            dealerState, dealerEmail, serviceId, addr, email, mileage, dealerPincode, pickup, vehicleNo, modelName, bookedDate, name,
            paystatus, slotDate, dealerPerson, dealerMob;
    ArrayList<String> VehicleRegnoList, vmodellist, dateTimeList, VehicleModelList, StatusList, ServiceIDList, MileageList, ServiceCenterList;
    CustomListAdapter adapter;
    ArrayList<String> DealerServiceCenterList;
    int bookingservice_count=0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.booking_details,container,false);

        SettingsUtils.init(getActivity());
        url = SettingsUtils.API_URL_BOOKING;

        VehicleRegnoList = new ArrayList<String>();
        dateTimeList = new ArrayList<String>();
        VehicleModelList = new ArrayList<String>();
        StatusList = new ArrayList<String>();
        ServiceIDList = new ArrayList<String>();
        ServiceCenterList = new ArrayList<String>();
        DealerServiceCenterList = new ArrayList<String>();
        vmodellist = new ArrayList<String>();

        listview=(ListView) v.findViewById(R.id.BookingListView);
        bookingService_count = (TextView)v.findViewById(R.id.bookingService_count);

        if(Connectivity.isConnected(getActivity())) {
            new JsonGetBookingService().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(getActivity());
        }

      /*  CustomListAdapter adapter=new CustomListAdapter(getActivity(), Brand, RegNum, Datetime, serviceCenter, serviceID, statusDetails);
        listview=(ListView)v.findViewById(R.id.BookingListView);
        listview.setAdapter(adapter);  */

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                String Slecteditem= RegNum[+position];
                   Toast.makeText(getActivity(), "Regno: " + Slecteditem, Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetBookingService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            VehicleRegnoList.clear();
            dateTimeList.clear();
            VehicleModelList.clear();
            StatusList.clear();
            ServiceIDList.clear();
            ServiceCenterList.clear();
            DealerServiceCenterList.clear();
            bookingservice_count=0;

            String gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "desc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "100"));
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getServHist"));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if (status.equals("success")) {

                        while (jObj_Keys.hasNext()) {
                            String jObj_key = (String) jObj_Keys.next();

                            if (jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                emptyData = jObj.get("data").toString();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    serviceType = tmpObj.get("stype").toString();
                                    dealerCountry = tmpObj.get("d_country").toString();
                                    mob = tmpObj.get("mob").toString();
                                    dealerCity = tmpObj.get("d_city").toString();
                                //    cmts = tmpObj.get("cmts").toString();
                                    dealerAddr = tmpObj.get("d_addr").toString();
                                    advance = tmpObj.get("advance").toString();
                                    dealerLocation = tmpObj.get("d_location").toString();
                                    slotTime = tmpObj.get("stime").toString();
                                    paymode = tmpObj.get("paymode").toString();
                                    dealerCompName = tmpObj.get("d_compname").toString();
                                    dealerState = tmpObj.get("d_state").toString();
                                    dealerEmail = tmpObj.get("d_email").toString();
                                    serviceId = tmpObj.get("sid").toString();
                                    addr = tmpObj.get("addr").toString();
                                    email = tmpObj.get("email").toString();
                                    mileage = tmpObj.get("mileage").toString();
                                    dealerPincode = tmpObj.get("d_pincode").toString();
                                    pickup = tmpObj.get("pickup").toString();
                                    vehicleNo = tmpObj.get("vno").toString();
                                    modelName = tmpObj.get("mname").toString();
                                    bookedDate = tmpObj.get("bdate").toString();
                                    name = tmpObj.get("name").toString();
                                    paystatus = tmpObj.get("paystatus").toString();
                                    slotDate = tmpObj.get("sdate").toString();
                                    dealerPerson = tmpObj.get("d_name").toString();
                                    dealerMob = tmpObj.get("d_mob").toString();
                                    bookstatus = tmpObj.get("status").toString();

                                    //Check greater and ct date and then display resch slot lists
                                    DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                                    Calendar cal2 = Calendar.getInstance();
                                    String currentdate = format2.format(cal2.getTime());
                                    Log.d("current DATE:", slotDate);
                                    if (slotDate.compareTo(currentdate) > 0) {
                                        System.out.println("greater than ctdate");
                                    }
                                    else {
                                        System.out.println("smaller than ctdate");
                                    }
//Check greater and ct date and then display resch slot lists according to Booked status

                                    if((slotDate.compareTo(currentdate) > 0)) {

                                        VehicleRegnoList.add(vehicleNo);
                                        dateTimeList.add(slotDate + "/" + slotTime);
                                        VehicleModelList.add(modelName);
                                        StatusList.add(bookstatus);
                                        ServiceIDList.add(serviceId);
                                        ServiceCenterList.add(dealerCompName);

                                        bookingservice_count = bookingservice_count + 1;

                                        System.out.println(tmpObj.get("sid"));
                                        System.out.println(tmpObj.get("d_compname") + "   bookstatus:  " + tmpObj.get("status"));

                                    }
                                    else if(((slotDate.compareTo(currentdate) < 0)) || (slotDate.equals(currentdate))) {

                                        if(bookstatus.equals("Booked")) {
                                            bookstatus = "No Show";
                                            Log.d("Bookstatus:", bookstatus);
                                        }
                                        else {
                                            Log.d("Bookstatus:", bookstatus);
                                        }

                                        VehicleRegnoList.add(vehicleNo);
                                        dateTimeList.add(slotDate + "/" + slotTime);
                                        VehicleModelList.add(modelName);
                                        StatusList.add(bookstatus);
                                        ServiceIDList.add(serviceId);
                                        ServiceCenterList.add(dealerCompName);

                                        bookingservice_count = bookingservice_count + 1;

                                        System.out.println(tmpObj.get("sid"));
                                        System.out.println(tmpObj.get("d_compname") + "   bookstatus:  " + tmpObj.get("status"));

                                    }

                                }
                            }
                        }

                    } else if (status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            RegNum = new String[VehicleRegnoList.size()];
            RegNum = VehicleRegnoList.toArray(RegNum);

            Datetime = new String[dateTimeList.size()];
            Datetime = dateTimeList.toArray(Datetime);

            Brand = new String[VehicleModelList.size()];
            Brand = VehicleModelList.toArray(Brand);

            statusDetails = new String[StatusList.size()];
            statusDetails = StatusList.toArray(statusDetails);

            serviceID = new String[ServiceIDList.size()];
            serviceID = ServiceIDList.toArray(serviceID);

            serviceCenter = new String[ServiceCenterList.size()];
            serviceCenter = ServiceCenterList.toArray(serviceCenter);

            boolean netconnection = NetworkAvailable.isNetworkAvailable(getActivity());
         //   Toast.makeText(getActivity(), "Internet connection:" + netconnection, Toast.LENGTH_LONG).show();

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(netconnection == true) {

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                        new JsonGetBookingService().execute();
                    }
                    else {
                        int length = serviceID.length;
                        if(! (length==0)) {   //!emptyData.equals("{}"
                            adapter = new CustomListAdapter(getActivity(), Brand, RegNum, Datetime, serviceCenter, serviceID, statusDetails);
                            listview.setAdapter(adapter);

                            bookingService_count.setText(String.valueOf(bookingservice_count) + " Services");
                        }
                        else {
                            showResultDialog("You have not yet booked a service");
                        }
                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetBookingService().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

            }
            else {

                showResultDialogFail("Check your Internet Connection");
            }


        }
    }


    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            //  startActivity(new Intent(getActivity(), HomeActivity.class));

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            //     startActivity(new Intent(getActivity(), HomeActivity.class));

                        }
                    })
                    .show();
        }
    }

}
