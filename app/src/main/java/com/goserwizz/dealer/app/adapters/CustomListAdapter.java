package com.goserwizz.dealer.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.activities.TrackService;

/**
 * Created by Kaptas on 3/14/2016.
 */
public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    public final String[] Brand, regNO, dateTime, serviceCenter, serviceId, statusDetails;


    public CustomListAdapter(Activity context, String[] brand, String[] regno, String[] date,  String[] servicecenter,
                             String[] S_id, String[] status) {
        super(context, R.layout.book_details_list_items, regno);
        // TODO Auto-generated constructor stub

        this.context=context;

        this.Brand=brand;
        this.regNO=regno;
        this.dateTime=date;
        this.serviceCenter=servicecenter;
        this.serviceId=S_id;
        this.statusDetails=status;

    }

    // the ViewHolder class that saves the row's states, acts as Tag
    static class ViewHolder {

        TextView brand, regno, datetime, servicecenter, serviceid;
        ImageView vehicle_image;
        LinearLayout trackservice;
        Button statusText;
    }

        public View getView(final int position,View view,final ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.book_details_list_items, null, true);

        final ViewHolder viewHolder = new ViewHolder();

        viewHolder.brand = (TextView) rowView.findViewById(R.id.brand_name);
        viewHolder.regno = (TextView) rowView.findViewById(R.id.vehicle_reg_no);
        viewHolder.datetime = (TextView) rowView.findViewById(R.id.date_time);
        viewHolder.servicecenter = (TextView) rowView.findViewById(R.id.service_center_name);
        viewHolder.serviceid = (TextView) rowView.findViewById(R.id.service_id);
        viewHolder.vehicle_image = (ImageView) rowView.findViewById(R.id.vehicle_image);
        viewHolder.statusText = (Button) rowView.findViewById(R.id.statusBtn);
        viewHolder.trackservice = (LinearLayout) rowView.findViewById(R.id.track_service_Layout);

        rowView.setTag(viewHolder);
        rowView.setTag(R.id.brand_name, viewHolder.brand);
        rowView.setTag(R.id.vehicle_reg_no, viewHolder.regno);
        rowView.setTag(R.id.date_time, viewHolder.datetime);
        rowView.setTag(R.id.service_center_name, viewHolder.servicecenter);
        rowView.setTag(R.id.service_id, viewHolder.serviceid);
        rowView.setTag(R.id.vehicle_image, viewHolder.vehicle_image);
        rowView.setTag(R.id.statusBtn, viewHolder.statusText);
        rowView.setTag(R.id.track_service_Layout, viewHolder.trackservice);

        viewHolder.brand.setText(Brand[position]);
        viewHolder.regno.setText(regNO[position]);
        viewHolder.datetime.setText(dateTime[position]);
        viewHolder.servicecenter.setText(serviceCenter[position]);
        viewHolder.serviceid.setText(serviceId[position]);
        viewHolder.statusText.setText(statusDetails[position]);

           String statusValue =  statusDetails[position].toString();
            if(statusValue.equals("Booked") || statusValue.equals("No Show")) {
                viewHolder.statusText.setBackgroundColor(Color.parseColor("#1a53ff"));
            }
            else if(statusValue.equals("Vehicle Received") || statusValue.equals("Work In Progress") || statusValue.equals("Ready for Delivery")) {
                viewHolder.statusText.setBackgroundColor(Color.parseColor("#31B0D5"));
            }
            else if(statusValue.equals("Completed") || statusValue.equals("Vehicle Delivered")) {
                viewHolder.statusText.setBackgroundColor(Color.parseColor("#009933"));
            }
            else if(statusValue.equals("Cancelled")) {
                viewHolder.statusText.setBackgroundColor(Color.parseColor("#e62e00"));
            }


            viewHolder.trackservice.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {

                    int pos = position;
               //     Toast.makeText(context, "clicked " + pos, Toast.LENGTH_LONG).show();

                    String statusShow = statusDetails[position];

                    if(statusShow.equals("No Show") || statusShow.equals("Booked")) {

                        Log.d("Status:: Trackservice", statusShow);
                        Toast.makeText(context, statusShow, Toast.LENGTH_LONG).show();
                    }

                    else {

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SERVICEID, serviceId[position]);
                        Intent track = new Intent(parent.getContext(), TrackService.class);
                        parent.getContext().startActivity(track);


                    }

                }
            });


        return rowView;

    };

}


