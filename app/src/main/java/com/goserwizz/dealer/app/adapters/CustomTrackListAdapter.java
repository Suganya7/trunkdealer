package com.goserwizz.dealer.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.goserwizz.dealer.app.R;

/**
 * Created by Kaptas on 4/2/2016.
 */
public class CustomTrackListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    public final String[] datetime, city, trackorder;

    public CustomTrackListAdapter(Activity context, String[] datetime,
                                    String[] city, String[] trackorder) {
        super(context, R.layout.book_details_list_items, datetime);
        // TODO Auto-generated constructor stub

        this.context = context;

        this.datetime = datetime;
        this.city = city;
        this.trackorder = trackorder;

    }

    // the ViewHolder class that saves the row's states, acts as Tag
    static class ViewHolder {

        TextView datetime, city, trackorder;
       // LinearLayout view_bookingdetails;
    }

    public View getView(final int position, View view, final ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.trackservice_list, null, true);

        final ViewHolder viewHolder = new ViewHolder();

        viewHolder.datetime = (TextView) rowView.findViewById(R.id.T_Datetime);
        viewHolder.city = (TextView) rowView.findViewById(R.id.T_City);
        viewHolder.trackorder = (TextView) rowView.findViewById(R.id.T_trackorder);

        rowView.setTag(viewHolder);
        rowView.setTag(R.id.T_Datetime, viewHolder.datetime);
        rowView.setTag(R.id.T_City, viewHolder.city);
        rowView.setTag(R.id.T_trackorder, viewHolder.trackorder);

        viewHolder.datetime.setText(datetime[position]);
        viewHolder.city.setText(city[position]);
        viewHolder.trackorder.setText(trackorder[position]);

       /* viewHolder.view_bookingdetails.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                int pos = position;
                //      Toast.makeText(context, "clicked " + pos, Toast.LENGTH_LONG).show();
                Intent bookdetail = new Intent(parent.getContext(), BookingDetailActivity.class);
                parent.getContext().startActivity(bookdetail);

            }
        });
*/

        return rowView;

    }
}
