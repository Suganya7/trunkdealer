package com.goserwizz.dealer.app.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.dealer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.Connectivity;
import com.goserwizz.dealer.app.Util.NetworkAvailable;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.activities.HomeActivity;
import com.goserwizz.dealer.app.adapters.CustomListAdapter;
import com.goserwizz.dealer.app.adapters.CustomServiceListAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 4/2/2016.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    ListView listview;

    public static String[] serviceCenter ={};

    public static String[] pendingService ={};

    public static String[] bookedService ={};

    public static String[] DealerID ={};

    private ProgressDialog pDialog;
    // URL to get contacts JSON  3vikram.in
    private static String url;
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_GDID = "gd_id";
    private static final String TAG_SESSION = "session";
    private static final String TAG_FNAME = "fname";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_BDATE = "bdate";


    // Hashmap for ListView
    ArrayList<HashMap<String, String>> userList;
    String Email, Pwd, gd_id, session, digest, fname, status, resultmsg;
    String pid, lname, compname, vmake, vtype, dtype, stype, mob, email, logo, vmodel;
    String location, state, addr, longi, lat, country, city, amenity, name;
    String id, t_cnt, p_cnt;
    ArrayList<String> IDList, t_cntList, p_cntlList, snameList;
    CustomServiceListAdapter adapter;
    Button datePickerBtn;
    public static String selecteddate="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_fragment,container,false);

        SettingsUtils.init(getActivity());
        url = SettingsUtils.API_URL_SERVICECOUNT;

        IDList = new ArrayList<String>();
        t_cntList = new ArrayList<String>();
        p_cntlList = new ArrayList<String>();
        snameList = new ArrayList<String>();

        listview=(ListView)v.findViewById(R.id.HomeListView);
        datePickerBtn = (Button)v.findViewById(R.id.datePicker);

        datePickerBtn.setOnClickListener(this);

       /* CustomServiceListAdapter adapter=new CustomServiceListAdapter(getActivity(), serviceCenter, pendingService, bookedService);
        listview=(ListView)v.findViewById(R.id.HomeListView);
        listview.setAdapter(adapter);
        */

        /* Defaultly show the current date services for booked and pending counts */
        DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal2 = Calendar.getInstance();
        String currentdate = format2.format(cal2.getTime());

        if(selecteddate.equals("")) {

            selecteddate = currentdate;
            datePickerBtn.setText(selecteddate);

            if (Connectivity.isConnected(getActivity())) {
                new GetJSONHomeBookedPendingServices().execute();
            } else {
                Connectivity.showNoConnectionDialog(getActivity());
            }
        }
        else {
            Log.d("selecteddate","pickdate");
            datePickerBtn.setText(selecteddate);

            if (Connectivity.isConnected(getActivity())) {
                new GetJSONHomeBookedPendingServices().execute();
            } else {
                Connectivity.showNoConnectionDialog(getActivity());
            }
        }

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                String Slecteditem= serviceCenter[+position];
                //   Toast.makeText(getActivity(), "servicecenter: " + Slecteditem, Toast.LENGTH_SHORT).show();
            }
        });

        return v;

    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.datePicker:
                android.support.v4.app.DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

                break;

        }
    }

    /*Button onclick for date picker
    public void showDatePickerDialog(View v) {
        android.support.v4.app.DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
    }  */


    /**
     * Async task class to get json by making HTTP call
     * */
    public class GetJSONHomeBookedPendingServices extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            IDList.clear();
            t_cntList.clear();
            p_cntlList.clear();
            snameList.clear();

            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");
            pid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PID, "");


            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_BDATE, selecteddate));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getServCenters"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while (jObj_Keys.hasNext()) {
                            String jObj_key = (String) jObj_Keys.next();

                            if (jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);

                                    if(pid.equals("0")) {

                                    id = tmpObj.get("id").toString();
                                    t_cnt = tmpObj.get("t_cnt").toString();
                                    name = tmpObj.get("name").toString();
                                    p_cnt = tmpObj.get("p_cnt").toString();

                                    IDList.add(id);
                                    t_cntList.add(t_cnt);
                                    p_cntlList.add(p_cnt);
                                    snameList.add(name);
                                    }
                                    else {

                                        id = pid;
                                        t_cnt = tmpObj.get("t_cnt").toString();
                                        name = tmpObj.get("name").toString();
                                        p_cnt = tmpObj.get("p_cnt").toString();

                                        IDList.add(id);
                                        t_cntList.add(t_cnt);
                                        p_cntlList.add(p_cnt);
                                        snameList.add(name);
                                    }

                                    Log.d("ID::", id);
                                }
                            }
                        }
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            serviceCenter = new String[snameList.size()];
            serviceCenter = snameList.toArray(serviceCenter);

            pendingService = new String[p_cntlList.size()];
            pendingService = p_cntlList.toArray(pendingService);

            bookedService = new String[t_cntList.size()];
            bookedService = t_cntList.toArray(bookedService);

            DealerID =  new String[IDList.size()];
            DealerID = IDList.toArray(DealerID);

            boolean netconnection = NetworkAvailable.isNetworkAvailable(getActivity());

            String Connection = (Connectivity.isConnected(getActivity()) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if (netconnection == true) {

                if (Connection.equals("Passed")) {
                    if (result.equals("success")) {

                        if (resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                            new GetJSONHomeBookedPendingServices().execute();
                        } else {

                            int length = serviceCenter.length;
                            if (!(length == 0)) {   //!emptyData.equals("{}"
                                adapter = new CustomServiceListAdapter(getActivity(), serviceCenter, pendingService, bookedService, DealerID);
                                listview.setAdapter(adapter);

                            }
                            else {
                                showResultDialogFail("You dont any services on this day");
                            }
                        }
                        } else if (result.equals("fail")) {

                        if (resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                              new GetJSONHomeBookedPendingServices().execute();
                        } else {
                            showResultDialogFail(resultmsg);
                        }
                    }
                } else {

                    showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Check your Internet Connection");

                }

            } else {
                showResultDialogFail("Check your Internet Connection");
            }
        }
    }

    public void onBackPressed() {

        Log.d("Login Fragment ===>", "user pressed the back button");

        getActivity().moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


    //Show date picker dialog fragment

    public static class DatePickerFragment extends android.support.v4.app.DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
           /* TextView tv1= (TextView) getActivity().findViewById(R.id.textview1);
            tv1.setText("Year: "+view.getYear()+" Month: "+view.getMonth()+" Day: "+view.getDayOfMonth()); */

            int mon = month+1;

            String date = day+"/"+mon+"/"+ year;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
            Date testDate = null;
            try {
                testDate = sdf.parse(date);
            }catch(Exception ex){
                ex.printStackTrace();
            }
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
            selecteddate = formatter.format(testDate);

          //  SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SDATE, selectdate);

            Toast.makeText(getActivity(), selecteddate , Toast.LENGTH_LONG).show();

            Intent refresh = new Intent(getActivity(), HomeActivity.class);
            startActivity(refresh);
            getActivity().finish(); //


        }
    }


}
