package com.goserwizz.dealer.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.activities.BookingDetailActivity;

/**
 * Created by Kaptas on 4/2/2016.
 */
public class CustomServiceListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    public final String[] serviceCenter, pendingService, bookedService, IDList;

    public CustomServiceListAdapter(Activity context, String[] servicecenter,
                             String[] pending_service, String[] booked_service, String[] dealerID) {
        super(context, R.layout.book_details_list_items, servicecenter);
        // TODO Auto-generated constructor stub

        this.context = context;

        this.serviceCenter = servicecenter;
        this.pendingService = pending_service;
        this.bookedService = booked_service;
        this.IDList = dealerID;

    }

    // the ViewHolder class that saves the row's states, acts as Tag
    static class ViewHolder {

        TextView servicecenter, pendingservice, bookedservice;
        LinearLayout view_bookingdetails;
    }

    public View getView(final int position, View view, final ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView = inflater.inflate(R.layout.home_list_items, null, true);

        final ViewHolder viewHolder = new ViewHolder();

        viewHolder.servicecenter = (TextView) rowView.findViewById(R.id.H_Service_Center);
        viewHolder.pendingservice = (TextView) rowView.findViewById(R.id.H_PendingService);
        viewHolder.bookedservice = (TextView) rowView.findViewById(R.id.H_BookedService);
        viewHolder.view_bookingdetails = (LinearLayout) rowView.findViewById(R.id.View_Booking_details_lay);

        rowView.setTag(viewHolder);
        rowView.setTag(R.id.H_Service_Center, viewHolder.servicecenter);
        rowView.setTag(R.id.H_PendingService, viewHolder.pendingservice);
        rowView.setTag(R.id.H_BookedService, viewHolder.bookedservice);
        rowView.setTag(R.id.View_Booking_details_lay, viewHolder.view_bookingdetails);

        viewHolder.servicecenter.setText(serviceCenter[position]);
        viewHolder.pendingservice.setText(pendingService[position]);
        viewHolder.bookedservice.setText(bookedService[position]);

        final String dealerid = IDList[position];

        viewHolder.view_bookingdetails.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                int pos = position;
          //      Toast.makeText(context, "clicked " + pos, Toast.LENGTH_LONG).show();

                SettingsUtils.init(context);
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FILTER, "");

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_DID, dealerid);
                Intent bookdetail = new Intent(parent.getContext(), BookingDetailActivity.class);
                parent.getContext().startActivity(bookdetail);

            }
        });


        return rowView;

    }

}
