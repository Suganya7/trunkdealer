package com.goserwizz.dealer.app.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.activities.HomeActivity;
import com.goserwizz.dealer.app.activities.MainActivity;


public class RegisterFragment extends Fragment {

    EditText firstName, lastName, email, mobile, password;
    LinearLayout facebook, signup;

    public RegisterFragment() {
        // Required empty public constructor
    }

    private static final long SPLASH_TIME = 2000; //3 seconds
    Handler mHandler;
    Runnable mJumpRunnable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_register, container, false);


        firstName = (EditText) view.findViewById(R.id.reg_firstname);
        lastName = (EditText) view.findViewById(R.id.reg_lastname);
        email = (EditText) view.findViewById(R.id.reg_email);
        mobile = (EditText) view.findViewById(R.id.reg_mobile);
        signup = (LinearLayout) view.findViewById(R.id.SignupLayout);


        firstName.addTextChangedListener(new MyTextWatcher(firstName));
        lastName.addTextChangedListener(new MyTextWatcher(lastName));
        email.addTextChangedListener(new MyTextWatcher(email));
        mobile.addTextChangedListener(new MyTextWatcher(mobile));

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

/*
        TextView terms = (TextView) view.findViewById(R.id.sagreeText1);
        TextView condition = (TextView) view.findViewById(R.id.sagreeText2);
        TextView privacypolicy = (TextView) view.findViewById(R.id.sagreeText4);

        terms.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans = (Spannable) terms.getText();
        ClickableSpan clickSpan = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans.setSpan(clickSpan, 0, spans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//
        condition.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans1 = (Spannable) condition.getText();
        ClickableSpan clickSpan1 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans1.setSpan(clickSpan1, 0, spans1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
        privacypolicy.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans2 = (Spannable) privacypolicy.getText();
        ClickableSpan clickSpan2 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {

             startActivity(new Intent(getActivity(), PrivacyPolicy.class));
            }
        };
        spans2.setSpan(clickSpan2, 0, spans2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
*/

        return view;
    }


    /**
     * Validating form
     */
    private void submitForm() {

        if (!validateName()) {
            return;
        }

        if (!validateLastName()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        if (!validateMobileNum()) {
            return;
        }

        RegisterDialog();

        mJumpRunnable = new Runnable() {
            public void run() {

                Intent refresh = new Intent(getActivity(), MainActivity.class);
                startActivity(refresh);
                getActivity().finish();
               // startActivity(new Intent(getActivity(),MainActivity.class));

            }
        };
        mHandler = new Handler();
        mHandler.postDelayed(mJumpRunnable, SPLASH_TIME);



     /*   if (!validateName()) {
            return;
        }

        if (!validateLastName()) {
            return;
        }

        if (!validateMobileNum()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        Toast.makeText(getActivity(), "Thank You for register!", Toast.LENGTH_SHORT).show();

        startActivity(new Intent(getActivity(), HomeActivity.class)); */
    }

    private boolean validateName() {
        if (firstName.getText().toString().trim().isEmpty()) {
            firstName.setError(getString(R.string.err_msg_name));
            requestFocus(firstName);
            return false;
        } else {
           // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName() {
        if (lastName.getText().toString().trim().isEmpty()) {
            lastName.setError(getString(R.string.err_msg_lastname));
            requestFocus(lastName);
            return false;
        } else {
            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobileNum() {
        if (mobile.getText().toString().trim().isEmpty()) {
            mobile.setError(getString(R.string.err_msg_mobile));
            requestFocus(mobile);
            return false;
        } else {
            // firstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        String emailreg = email.getText().toString().trim();

        if (emailreg.isEmpty() || !isValidEmail(emailreg)) {
            email.setError(getString(R.string.err_msg_email));
            requestFocus(email);
            return false;
        } else {
            //email.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty()) {
            password.setError(getString(R.string.err_msg_password));
            requestFocus(password);
            return false;
        } else {
          //  password.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.reg_firstname:
                    validateName();
                    break;
                case R.id.reg_lastname:
                    validateLastName();
                    break;
                case R.id.reg_mobile:
                    validateMobileNum();
                    break;
                case R.id.reg_email:
                    validateEmail();
                    break;
              /*  case R.id.reg_pwd:
                    validatePassword();
                    break;  */
            }
        }
    }

/* Register Dialog */

    public void RegisterDialog() {

        final Dialog regDialog = new Dialog(getActivity());
        regDialog.setContentView(R.layout.register_dialog);
        ImageView menu_logo = (ImageView)regDialog.findViewById(R.id.reg_menu_logo);

        menu_logo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                regDialog.dismiss();
                startActivity(new Intent(getActivity(), HomeActivity.class));
            }
        });
        regDialog.show();


    }

}
