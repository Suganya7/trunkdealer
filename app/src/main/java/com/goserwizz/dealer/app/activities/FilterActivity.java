package com.goserwizz.dealer.app.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.dealer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.Connectivity;
import com.goserwizz.dealer.app.Util.NetworkAvailable;
import com.goserwizz.dealer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 5/18/2016.
 */
public class FilterActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    LinearLayout datelay, modellay, servicelay, typelay, statuslay;
    LinearLayout calDatelay, ModelLayout, ServiceCenterlay, ServiceTypelay, ServiceStatuslay;
    CalendarView calendarView;

    TextView date,model, center, type, status, ResetText, filterText;
    ImageView filtercancel, filterdone;
    LinearLayout filter_header_layout, ResetLayout;
    ListView all_models_list, service_center_list, service_type_list, service_status_list;
    private String[] Modelarr = {};
    private String[] ModelIDarr = {};
    private String[] ServiceCenterarr = {"Trident Hyundai-Jakksandra", "Trident Hyundai-Whitefields"};
    private String[] ServiceTypearr = {"First Free Service", "Second Free Service", "Third Free Service", "Paid Service", "Running Repairs", "Body Shop"};
    private String[] ServiceStatusarr = {"Booked", "Vehicle Received", "Work In Progress", "Ready for Delivery", "Vehicle Delivered", "Completed", "Cancelled"};
    private String[] ServiceIDarr = {};
    private String[] ServiceNamearr = {};

    private static String url, url2;

    private ProgressDialog pDialog, Dialog;
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_GDID = "gd_id";
    private static final String TAG_VTYPE = "vtype";
    private static final String TAG_VMAKE = "vmake";
    private static final String TAG_START = "start";
    private static final String TAG_COUNT = "cnt";
    private static final String TAG_ORDER = "order";

    private static final String TAG_SSTATUS = "sstatus";
    private static final String TAG_STYPE = "stype";
    private static final String TAG_VMODEL = "vmodel";
    private static final String TAG_SDATE = "sdate";
    private static final String TAG_DID = "did";

    String statusresult, resultmsg, gd_id, emptyData, bookstatus, vtype, vmake;
    String serviceType, dealerCountry, mob, dealerCity, cmts, dealerAddr, advance, dealerLocation, slotTime, paymode, dealerCompName,
            dealerState, dealerEmail, serviceId, addr, email, mileage, dealerPincode, pickup, vehicleNo, modelName, bookedDate, name,
            paystatus, slotDate, dealerPerson, dealerMob;
    String vmodel, sdate, sstatus, stype, did, serviceCenterID, serviceCenterName;

    ArrayList<String> VehicleModelList, VModelIDList, ServiceID, ServiceName;
    ArrayList<String> VehicleRegnoList, dateTimeList, StatusList, ServiceIDList, ServiceCenterList;
    int bookingservice_count=0;
    String pid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filters_activity);

        SettingsUtils.init(this);
        VehicleModelList = new ArrayList<String>();
        VModelIDList = new ArrayList<String>();
        VehicleRegnoList = new ArrayList<String>();
        dateTimeList = new ArrayList<String>();
        VehicleModelList = new ArrayList<String>();
        StatusList = new ArrayList<String>();
        ServiceIDList = new ArrayList<String>();
        ServiceCenterList = new ArrayList<String>();
        ServiceID = new ArrayList<String>();
        ServiceName = new ArrayList<String>();

        url = SettingsUtils.API_URL_VMODEL;
        url2 = SettingsUtils.API_URL_FILTER;

        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SDATE, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODELID, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SSTYPE, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SSTATUS, "");
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SCENTER, "");

        datelay = (LinearLayout) findViewById(R.id.datelay);
        modellay = (LinearLayout) findViewById(R.id.modellay);
        servicelay = (LinearLayout) findViewById(R.id.scenterlay);
        typelay = (LinearLayout) findViewById(R.id.stypelay);
        statuslay = (LinearLayout) findViewById(R.id.sstatuslay);
        calDatelay = (LinearLayout) findViewById(R.id.CalendarDateLayout);
        ServiceCenterlay = (LinearLayout) findViewById(R.id.Service_Center_Layout);
        ServiceTypelay = (LinearLayout) findViewById(R.id.Service_Type_Layout);
        ServiceStatuslay = (LinearLayout) findViewById(R.id.Service_Status_Layout);
        ModelLayout = (LinearLayout) findViewById(R.id.ModelLayout);
        date = (TextView) findViewById(R.id.dateText);
        model = (TextView) findViewById(R.id.modelText);
        center = (TextView) findViewById(R.id.scenterText);
        type = (TextView) findViewById(R.id.stypeText);
        status = (TextView) findViewById(R.id.sstatusText);
        filtercancel = (ImageView) findViewById(R.id.filterCancel);
        filterdone = (ImageView) findViewById(R.id.FilterDoneImage);
        filterText = (TextView) findViewById(R.id.filterText);
        ResetLayout = (LinearLayout) findViewById(R.id.ResetLayout);
        all_models_list = (ListView)findViewById(R.id.all_models_list);
        service_center_list = (ListView)findViewById(R.id.service_center_list);
        service_type_list = (ListView)findViewById(R.id.service_type_list);
        service_status_list = (ListView)findViewById(R.id.service_status_list);
        calendarView = (CalendarView)findViewById(R.id.calendarView);
        ResetText = (TextView)findViewById(R.id.ResetText);

        datelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.VISIBLE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#cc0000"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#FFFFFF"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#000000"));
            }
        });

        modellay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.VISIBLE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#cc0000"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#FFFFFF"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#000000"));

            }
        });

        servicelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.VISIBLE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#cc0000"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#FFFFFF"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#000000"));
            }
        });

        typelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.GONE);
                ServiceTypelay.setVisibility(View.VISIBLE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#cc0000"));
                statuslay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#FFFFFF"));
                status.setTextColor(Color.parseColor("#000000"));
            }
        });

        statuslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calDatelay.setVisibility(View.GONE);
                ModelLayout.setVisibility(View.GONE);
                ServiceCenterlay.setVisibility(View.GONE);
                ServiceStatuslay.setVisibility(View.VISIBLE);
                ServiceTypelay.setVisibility(View.GONE);

                datelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                modellay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                servicelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                typelay.setBackgroundColor(Color.parseColor("#e6e6e6"));
                statuslay.setBackgroundColor(Color.parseColor("#cc0000"));
                date.setTextColor(Color.parseColor("#000000"));
                model.setTextColor(Color.parseColor("#000000"));
                center.setTextColor(Color.parseColor("#000000"));
                type.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });


        if(Connectivity.isConnected(this)) {
            new JsonGetVModel().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }

        pid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PID, "");

        if(pid.equals("0")) {

            servicelay.setVisibility(View.VISIBLE);

            if(Connectivity.isConnected(this)) {
                new JsonGetServiceCenterforBusinessHead().execute();
            }
            else {
                Connectivity.showNoConnectionDialog(this);
            }
        }
        else {

            servicelay.setVisibility(View.GONE);

            Log.d("PID for service center", pid);
        }



        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                // TODO Auto-generated method stub

                int mon = month+1;

                String date = dayOfMonth+"/"+mon+"/"+ year;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
                Date testDate = null;
                try {
                    testDate = sdf.parse(date);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
                String selectdate = formatter.format(testDate);

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SDATE, selectdate);
                Toast.makeText(getApplicationContext(), selectdate , Toast.LENGTH_SHORT).show();

            }
        });


        ////**** Get All Models data ****////
        all_models_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        //all_models_list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, Modelarr));
        all_models_list.setOnItemClickListener(this);

        ////**** Get All Service Center data ****////
        service_center_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
     //   service_center_list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, ServiceCenterarr));
        service_center_list.setOnItemClickListener(this);

        ////**** Get All Service Type data ****////
        service_type_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        service_type_list.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, ServiceTypearr));
        service_type_list.setOnItemClickListener(this);

        ////**** Get All Service Status data ****////
        service_status_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        service_status_list.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, ServiceStatusarr));
        service_status_list.setOnItemClickListener(this);

        ////**** Filter Cancel click function ****////
        filtercancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterCancelDialog();
            }
        });

        filterText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterCancelDialog();
            }
        });

        ////**** Filter DONE click function ****////
        filterdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean netconnection = NetworkAvailable.isNetworkAvailable(FilterActivity.this);
           //     Toast.makeText(FilterActivity.this, "Internet connection:" + netconnection, Toast.LENGTH_LONG).show();

                if(netconnection == true) {

                if(Connectivity.isConnected(FilterActivity.this)) {
                    new GetJsonByFilter().execute();
                }
                else {
                    Connectivity.showNoConnectionDialog(FilterActivity.this);
                }

                }
                else {
                    Connectivity.showNoConnectionDialog(FilterActivity.this);
                }

            }
        });

        ////**** Reset Remove all check on click function ****////
        ResetText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i=0 ; i < all_models_list.getAdapter().getCount(); i++)
                {
                    all_models_list.setItemChecked(i, false);
                }

               if(pid.equals("0")) {
                   for (int i = 0; i < service_center_list.getAdapter().getCount(); i++) {
                       service_center_list.setItemChecked(i, false);
                   }
               }

                for(int i=0 ; i < service_type_list.getAdapter().getCount(); i++)
                {
                    service_type_list.setItemChecked(i, false);
                }

                for(int i=0 ; i < service_status_list.getAdapter().getCount(); i++)
                {
                    service_status_list.setItemChecked(i, false);
                }

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SDATE, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODELID, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SSTYPE, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SSTATUS, "");
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SCENTER, "");

            }
        });


    }

    @Override
    public void onItemClick(AdapterView<?> parent,
                            View view, int position, long id) {

        switch(parent.getId()){
            case R.id.all_models_list:
                // write what you need here when the user clicks on the first list item
                String item = Modelarr[position];
                String itemid = ModelIDarr[position];
                Toast.makeText(FilterActivity.this, item, Toast.LENGTH_SHORT).show();

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMODELID, itemid);
                break;

            case R.id.service_center_list:
                // write what you need here when the user clicks on the 2nd list item
                String itemn2 = ServiceNamearr[position];
                String item2 = ServiceIDarr[position];
                Toast.makeText(FilterActivity.this, itemn2, Toast.LENGTH_SHORT).show();

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SCENTER, item2);

                break;

            case R.id.service_type_list:
                // write what you need here when the user clicks on the 2nd list item
                String item3 = ServiceTypearr[position];
                Toast.makeText(FilterActivity.this, item3, Toast.LENGTH_SHORT).show();

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SSTYPE, item3);

                break;

            case R.id.service_status_list:
                // write what you need here when the user clicks on the 2nd list item
                String item4 = ServiceStatusarr[position];
                Toast.makeText(FilterActivity.this, item4, Toast.LENGTH_SHORT).show();

                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_SSTATUS, item4);
                break;
        }
    };


    public void FilterCancelDialog() {

        final Dialog filterDialog = new Dialog(this);
        filterDialog.setContentView(R.layout.custom_dialog);
        //  filterDialog.setTitle("Custom Dialog Box");

        TextView applyfilter = (TextView)filterDialog.findViewById(R.id.apply_filters);
        TextView continuefilter = (TextView)filterDialog.findViewById(R.id.continue_filters);

        applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                filterDialog.dismiss();
                //  Toast.makeText(FiltersActivity.this, "You clicked APPLYFILTERS", Toast.LENGTH_LONG).show();
            }
        });
        continuefilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                filterDialog.dismiss();

                Intent i = new Intent(FilterActivity.this, HomeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

              //  startActivity(new Intent(FilterActivity.this, HomeActivity.class));
            }
        });
        filterDialog.show();

    }

    private void removeAllChecks(ViewGroup vg) {
        View v = null;
        for(int i = 0; i < vg.getChildCount(); i++){
            try {
                v = vg.getChildAt(i);
                ((CheckBox)v).setChecked(false);
            }
            catch(Exception e1){ //if not checkBox, null View, etc
                try {
                    removeAllChecks((ViewGroup)v);
                }
                catch(Exception e2){ //v is not a view group
                    continue;
                }
            }
        }

    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetVModel extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(FilterActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            VehicleModelList.clear();
            VModelIDList.clear();

            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");
            vmake = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMAKE, "");
            vtype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VTYPE, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_VTYPE, vtype));
            nameValuePairs.add(new BasicNameValuePair(TAG_VMAKE, vmake));
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getvmodel"));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    statusresult = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if (statusresult.equals("success")) {

                        while (jObj_Keys.hasNext()) {
                            String jObj_key = (String) jObj_Keys.next();

                            if (jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                emptyData = jObj.get("data").toString();

                              /*  for(int i=0; i<dataObj.length(); i++) {

                                    String modelName = dataObj.get("116").toString();
                                    System.out.println("dataobj modelName"+ modelName);
                                }  */

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    modelName = dataObj.get(dataObj_key).toString();

                                    VehicleModelList.add(modelName);
                                    VModelIDList.add(dataObj_key);


                                    System.out.println(dataObj_key + "," + modelName);

                                }
                            }
                        }

                    } else if (statusresult.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return statusresult;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            Modelarr = new String[VehicleModelList.size()];
            Modelarr = VehicleModelList.toArray(Modelarr);

            ModelIDarr = new String[VModelIDList.size()];
            ModelIDarr = VModelIDList.toArray(ModelIDarr);

            String Connection = (Connectivity.isConnected(FilterActivity.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                        new JsonGetVModel().execute();
                    }
                    else {
                        int length = Modelarr.length;
                        if(! (length==0)) {   //!emptyData.equals("{}"

                            all_models_list.setAdapter(new ArrayAdapter<String>(FilterActivity.this,
                                    android.R.layout.simple_list_item_multiple_choice, Modelarr));
                        }
                        else {
                            showResultDialog("You dont have any model items");
                        }
                    }


                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetVModel().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetServiceCenterforBusinessHead extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            Dialog = new ProgressDialog(FilterActivity.this);
            Dialog.setMessage("Loading...");
            Dialog.setCancelable(false);
            Dialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            ServiceID.clear();
            ServiceName.clear();

            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getServCenters"));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url2, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    statusresult = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if (statusresult.equals("success")) {

                        while (jObj_Keys.hasNext()) {
                            String jObj_key = (String) jObj_Keys.next();

                            if (jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    serviceCenterID = tmpObj.get("id").toString();
                                    serviceCenterName = tmpObj.get("name").toString();

                                    ServiceID.add(serviceCenterID);
                                    ServiceName.add(serviceCenterName);

                                    System.out.println("ServiceCenter:: , " + serviceCenterID + ", "+ serviceCenterName);

                                }
                            }
                        }

                    } else if (statusresult.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return statusresult;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (Dialog.isShowing()) {
                Dialog.dismiss();
            }
            /**
             * Updating parsed JSON data into ListView
             * */

            ServiceIDarr = new String[ServiceID.size()];
            ServiceIDarr = ServiceID.toArray(ServiceIDarr);

            ServiceNamearr = new String[ServiceName.size()];
            ServiceNamearr = ServiceName.toArray(ServiceNamearr);

            String Connection = (Connectivity.isConnected(FilterActivity.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                        new JsonGetServiceCenterforBusinessHead().execute();
                    }
                    else {
                        int length = ServiceIDarr.length;
                        if(! (length==0)) {   //!emptyData.equals("{}"

                            service_center_list.setAdapter(new ArrayAdapter<String>(FilterActivity.this,
                                    android.R.layout.simple_list_item_multiple_choice, ServiceNamearr));
                        }
                        else {
                            showResultDialog("You dont have any Service Center");
                        }
                    }

                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                        //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                        new JsonGetServiceCenterforBusinessHead().execute();
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class GetJsonByFilter extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(FilterActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            VehicleRegnoList.clear();
            dateTimeList.clear();
            StatusList.clear();
            ServiceIDList.clear();
            ServiceCenterList.clear();

            VehicleModelList.clear();
            VModelIDList.clear();
            serviceId = "";
            bookingservice_count=0;


            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");
            vmodel = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_VMODELID, "");
            sdate = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SDATE, "");
            sstatus = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SSTATUS, "");
            stype = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SSTYPE, "");
            did = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SCENTER, "");


            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            /* Below parameters are needed according to the filter selected by dealer*/
            /* 1 combination for 5 numbers */
            if(!vmodel.equals("") && !sstatus.equals("") && !stype.equals("") && !sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            /* 5 combinations for 4 numbers in each module*/
            else if(!vmodel.equals("") && !sstatus.equals("") && !stype.equals("") && !sdate.equals("")) {

                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!vmodel.equals("") && !stype.equals("") && !sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!sstatus.equals("") && !stype.equals("") && !sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!sdate.equals("") && !did.equals("") && !vmodel.equals("") && !sstatus.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!did.equals("") && !vmodel.equals("") && !sstatus.equals("") && !stype.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            /* 10 combinations for 3 numbers in each module*/
            else if(!vmodel.equals("") && !sstatus.equals("") && !stype.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
            }
            else if(!vmodel.equals("") && !stype.equals("") && !sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!vmodel.equals("") && !sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!sstatus.equals("") && !stype.equals("") && !sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!sstatus.equals("") && !sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!stype.equals("") && !sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!stype.equals("") && !sstatus.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!stype.equals("") && !vmodel.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!sstatus.equals("") && !vmodel.equals("") && !sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!sstatus.equals("") && !vmodel.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            /* 10 combinations for 2 numbers in each module*/
            else if(!vmodel.equals("") && !sstatus.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
            }
            else if(!vmodel.equals("") && !stype.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
            }
            else if(!vmodel.equals("") && !sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!vmodel.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!sstatus.equals("") && !stype.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
            }
            else if(!sstatus.equals("") && !sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!sstatus.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!stype.equals("") && !sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!stype.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }
            else if(!sdate.equals("") && !did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }

            /* single values in each module*/
            else if(!vmodel.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, vmodel));
            }
            else if(!stype.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, stype));
            }
            else if(!sstatus.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, sstatus));
            }
            else if(!sdate.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, sdate));
            }
            else if(!did.equals("")) {
                nameValuePairs.add(new BasicNameValuePair(TAG_DID, did));
            }

           /* nameValuePairs.add(new BasicNameValuePair(TAG_SDATE, "2016-05-28"));
            nameValuePairs.add(new BasicNameValuePair(TAG_VMODEL, "114"));
            nameValuePairs.add(new BasicNameValuePair(TAG_STYPE, "Second Free Service"));
            nameValuePairs.add(new BasicNameValuePair(TAG_SSTATUS, "Vehicle Delivered"));
         //   nameValuePairs.add(new BasicNameValuePair(TAG_DID, "106"));
            /* Above parameters are needed according to the filter selected by dealer*/


            /* Below parameters are needed for all the filters */
            nameValuePairs.add(new BasicNameValuePair(TAG_ORDER, "asc"));
            nameValuePairs.add(new BasicNameValuePair(TAG_START, "1"));
            nameValuePairs.add(new BasicNameValuePair(TAG_COUNT, "100"));
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getServHist"));

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url2, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    statusresult = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if (statusresult.equals("success")) {

                        while (jObj_Keys.hasNext()) {
                            String jObj_key = (String) jObj_Keys.next();

                            if (jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    serviceType = tmpObj.get("stype").toString();
                                    dealerCountry = tmpObj.get("d_country").toString();
                                    mob = tmpObj.get("mob").toString();
                                    dealerCity = tmpObj.get("d_city").toString();
                             //       cmts = tmpObj.get("cmts").toString();
                                    dealerAddr = tmpObj.get("d_addr").toString();
                                    advance = tmpObj.get("advance").toString();
                                    dealerLocation = tmpObj.get("d_location").toString();
                                    slotTime = tmpObj.get("stime").toString();
                                    paymode = tmpObj.get("paymode").toString();
                                    dealerCompName = tmpObj.get("d_compname").toString();
                                    dealerState = tmpObj.get("d_state").toString();
                                    dealerEmail = tmpObj.get("d_email").toString();
                                    serviceId = tmpObj.get("sid").toString();
                                    addr = tmpObj.get("addr").toString();
                                    email = tmpObj.get("email").toString();
                                    mileage = tmpObj.get("mileage").toString();
                                    dealerPincode = tmpObj.get("d_pincode").toString();
                                    pickup = tmpObj.get("pickup").toString();
                                    vehicleNo = tmpObj.get("vno").toString();
                                    modelName = tmpObj.get("mname").toString();
                                    bookedDate = tmpObj.get("bdate").toString();
                                    name = tmpObj.get("name").toString();
                                    paystatus = tmpObj.get("paystatus").toString();
                                    slotDate = tmpObj.get("sdate").toString();
                                    dealerPerson = tmpObj.get("d_name").toString();
                                    dealerMob = tmpObj.get("d_mob").toString();
                                    bookstatus = tmpObj.get("status").toString();

                                    //Check greater and ct date and then display resch slot lists
                                    DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
                                    Calendar cal2 = Calendar.getInstance();
                                    String currentdate = format2.format(cal2.getTime());
                                    Log.d("current DATE:", slotDate);
                                    if (slotDate.compareTo(currentdate) > 0) {
                                        System.out.println("greater than ctdate");
                                    }
                                    else {
                                        System.out.println("smaller than ctdate");
                                    }
//Check greater and ct date and then display resch slot lists according to Booked status

                                    if((slotDate.compareTo(currentdate) > 0)) {

                                        VehicleRegnoList.add(vehicleNo);
                                        dateTimeList.add(slotDate + "/" + slotTime);
                                        VehicleModelList.add(modelName);
                                        StatusList.add(bookstatus);
                                        ServiceIDList.add(serviceId);
                                        ServiceCenterList.add(dealerCompName);

                                        bookingservice_count = bookingservice_count + 1;

                                        System.out.println(tmpObj.get("sid"));
                                        System.out.println(tmpObj.get("d_compname") + "   bookstatus:  " + tmpObj.get("status"));

                                    }
                                    else if(((slotDate.compareTo(currentdate) < 0)) || (slotDate.equals(currentdate))) {

                                        if(bookstatus.equals("Booked")) {
                                            bookstatus = "No Show";
                                            Log.d("Bookstatus:", bookstatus);
                                        }
                                        else {
                                            Log.d("Bookstatus:", bookstatus);
                                        }

                                        VehicleRegnoList.add(vehicleNo);
                                        dateTimeList.add(slotDate + "/" + slotTime);
                                        VehicleModelList.add(modelName);
                                        StatusList.add(bookstatus);
                                        ServiceIDList.add(serviceId);
                                        ServiceCenterList.add(dealerCompName);

                                        bookingservice_count = bookingservice_count + 1;

                                        System.out.println(tmpObj.get("sid"));
                                        System.out.println(tmpObj.get("d_compname") + "   bookstatus:  " + tmpObj.get("status"));

                                    }

                                }
                            }
                        }

                    } else if (statusresult.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }

            return statusresult;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            boolean netconnection = NetworkAvailable.isNetworkAvailable(FilterActivity.this);
        //    Toast.makeText(FilterActivity.this, "Internet connection:" + netconnection, Toast.LENGTH_LONG).show();


            String Connection = (Connectivity.isConnected(FilterActivity.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(netconnection == true) {

                if (Connection.equals("Passed")) {

                    if (result.equals("success")) {

                        if (resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                            new GetJsonByFilter().execute();
                        } else {


                            int length = VehicleRegnoList.size();
                            if (!(length == 0)) {   //!emptyData.equals("{}"

                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FILTER, "Filter");
                                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_BOOKINGserviceCount, String.valueOf(bookingservice_count) + " Services");
                                SettingsUtils.getInstance().putListString(SettingsUtils.KEY_RegNum, VehicleRegnoList);
                                SettingsUtils.getInstance().putListString(SettingsUtils.KEY_dateTime, dateTimeList);
                                SettingsUtils.getInstance().putListString(SettingsUtils.KEY_vehicleModel, VehicleModelList);
                                SettingsUtils.getInstance().putListString(SettingsUtils.KEY_statuss, StatusList);
                                SettingsUtils.getInstance().putListString(SettingsUtils.KEY_serviceID, ServiceIDList);
                                SettingsUtils.getInstance().putListString(SettingsUtils.KEY_serviceCenter, ServiceCenterList);

                                startActivity(new Intent(FilterActivity.this, BookingDetailActivity.class));

                            } else {
                                showResultDialog("You dont have any items on this filter");
                            }
                        }


                    } else if (result.equals("fail")) {

                        if (resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {
                            //   showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                            new GetJsonByFilter().execute();
                        } else {
                            showResultDialogFail(resultmsg);
                        }
                    }

                } else {
                    showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
                }
            }
            else {
                showResultDialogFail("Check your Internet Connection");
            }

        }
    }

    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            //  startActivity(new Intent(getActivity(), HomeActivity.class));

                        }
                    })
                    .setNegativeButton("Cancel", null)
                    .show();
        }
    }

    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);
                            //     startActivity(new Intent(getActivity(), HomeActivity.class));

                        }
                    })
                    .show();
        }
    }


}
