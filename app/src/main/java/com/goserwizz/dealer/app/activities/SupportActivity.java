package com.goserwizz.dealer.app.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.goserwizz.dealer.app.R;

/**
 * Created by Kaptas on 4/4/2016.
 */
public class SupportActivity extends AppCompatActivity {

    LinearLayout supportlay, feedbacklay;
    ImageView supportCancel;

    // Assume thisActivity is the current activity
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.supportactivity);

        supportlay = (LinearLayout)findViewById(R.id.supportLay);
        feedbacklay = (LinearLayout)findViewById(R.id.feedbackLay);
        supportCancel = (ImageView) findViewById(R.id.supportCancel);


        if (!checkPhoneCallPermission()) {

            requesPhoneCallPermission();

        } else {

            Log.d("Permission granted", "Access call phone");

            //   Toast.makeText(ServiceCenter.this, "Permission already granted.", Toast.LENGTH_LONG).show();

        }



        supportCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SupportActivity.this, HomeActivity.class));

            }
        });

        supportlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( Build.VERSION.SDK_INT >= 23 &&
                        ContextCompat.checkSelfPermission( SupportActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                }

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+"+91 9663544674"));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);

            }
        });

        feedbacklay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SupportActivity.this, FeedbackActivity.class));

            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], final int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            Log.d("Location::", "Permission Granted, Now you can access ext data.");
                            //   Toast.makeText(ServiceCenter.this, "Permission Granted, Now you can access location data.", Toast.LENGTH_LONG).show();


                        } else {
                            Log.d("Location::", "Permission Denied, You cannot access ext data.");
                            //  Toast.makeText(ServiceCenter.this, "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();

                        }

                    }
                });


                break;
        }
    }


    private boolean checkPhoneCallPermission(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED){

            return true;

        } else {

            return false;

        }
    }

    private void requesPhoneCallPermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)){

            Toast.makeText(SupportActivity.this, "sdcard allows us to access file data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.CALL_PHONE},MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }
    }



}