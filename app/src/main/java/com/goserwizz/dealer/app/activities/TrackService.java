package com.goserwizz.dealer.app.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.dealer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.Connectivity;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.adapters.CustomTrackListAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/31/2016.
 */
public class TrackService extends AppCompatActivity {

    public static String[] Datetime ={};
    public static String[] City ={};
    public static String[] TrackOrder ={};

    ListView listview;
    ImageView cancel;
    TextView servicecenterName, location, ServiceId;

    private ProgressDialog pDialog;
    private static String url;
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_GDID = "gd_id";
    private static final String TAG_MSG = "msg";
    private static final String TAG_SID = "sid";
    private static final String TAG_STATUS = "status";

    String gd_id, s_id, status, resultmsg;
    String mname, city, sid, tstatus, scmts, date, by;
    ArrayList<String> DateTimeList, DealerCityList, TrackStatusList;
    CustomTrackListAdapter adapter;
    LinearLayout TrackLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_service);

        SettingsUtils.init(this);
        url = SettingsUtils.API_URL_BOOKING;

        DateTimeList = new ArrayList<String>();
        DealerCityList = new ArrayList<String>();
        TrackStatusList = new ArrayList<String>();

        cancel = (ImageView)findViewById(R.id.CancelTrack);
        listview=(ListView) findViewById(R.id.trackListView);
        servicecenterName = (TextView) findViewById(R.id.ServiceCenterName);
        ServiceId = (TextView) findViewById(R.id.serviceID);
        location = (TextView) findViewById(R.id.Location);
        TrackLayout = (LinearLayout) findViewById(R.id.TrackLayout);

        cancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                finish();
            }
        });

        //*****  Call the Get Track Service function   ****//

        if(Connectivity.isConnected(this)) {

            new JSONGetTrackService().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }


       /* adapter=new CustomTrackListAdapter(this, Datetime, City, TrackOrder);
        listview=(ListView) findViewById(R.id.trackListView);
        listview.setAdapter(adapter);  */

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                String Slecteditem = TrackOrder[+position];
        //        Toast.makeText(getApplicationContext(), "TrackOrder: "+Slecteditem, Toast.LENGTH_SHORT).show();
            }
        });

    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class JSONGetTrackService extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(TrackService.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            DateTimeList.clear();
            DealerCityList.clear();
            TrackStatusList.clear();

            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");
            s_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_SERVICEID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_SID, s_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getServDet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


            if(status.equals("success")) {

                while (jObj_Keys.hasNext()) {
                    String jObj_key = (String) jObj_Keys.next();

                if (jObj_key.equals("data")) {
                    JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                    Iterator<?> dataObj_keys = dataObj.keys();

                    while (dataObj_keys.hasNext()) {
                        String dataObj_key = (String) dataObj_keys.next();

                        JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);

                            mname = tmpObj.get("mname").toString();
                            city = tmpObj.get("d_city").toString();
                            sid = tmpObj.get("sid").toString();

                            JSONObject trackdataObj = (JSONObject) dataObj.get(dataObj_key);
                            Iterator<?> trackdataObj_keys = trackdataObj.keys();

                            while (trackdataObj_keys.hasNext()) {
                                String dataObj_key1 = (String) trackdataObj_keys.next();

                                if(dataObj_key1.equals("shist")) {
                                    JSONObject dataObj1 = (JSONObject) trackdataObj.get(dataObj_key1);
                                    Iterator<?> dataObj_keys1 = dataObj1.keys();

                                    while (dataObj_keys1.hasNext()) {
                                        String dataObj_key2 = (String) dataObj_keys1.next();

                                        JSONObject tmpObj1 = (JSONObject) dataObj1.get(dataObj_key2);

                                        tstatus = tmpObj1.get("status").toString();
                                 //      scmts = tmpObj1.get("scmts").toString();
                                        date = tmpObj1.get("date").toString();
                                 //       by = tmpObj1.get("by").toString();


                                        DateTimeList.add(date);
                                        DealerCityList.add(city);
                                        TrackStatusList.add(tstatus);

                                        Log.d("TrackService date ::", date + "  status:: " + tstatus);
                                      }
                                    }
                                 }

                                    Log.d("TrackSID::", sid + " city:: " + city + " model:: " + mname);
                                }
                            }
                        }
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();

            TrackLayout.setVisibility(View.VISIBLE);

            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {

                if(resultmsg.contains("Communications link failure") || result.contains("The last packet")) {

                    new JSONGetTrackService().execute();
                }
                else {

                    Datetime = new String[DateTimeList.size()];
                    Datetime = DateTimeList.toArray(Datetime);

                    City = new String[DealerCityList.size()];
                    City = DealerCityList.toArray(City);

                    TrackOrder = new String[TrackStatusList.size()];
                    TrackOrder = TrackStatusList.toArray(TrackOrder);

                    adapter=new CustomTrackListAdapter(TrackService.this, Datetime, City, TrackOrder);
                    listview=(ListView) findViewById(R.id.trackListView);
                    listview.setAdapter(adapter);

                    servicecenterName.setText(mname);
                    ServiceId.setText("Service ID: "+ sid);
                    location.setText(city);

                   // Toast.makeText(TrackService.this, resultmsg, Toast.LENGTH_LONG).show();

                }

            }
            else if(result.equals("fail")) {

                if(resultmsg.contains("Communications link failure") || result.contains("The last packet")) {

                    new JSONGetTrackService().execute();
                }
                else {
                    showResultDialogFail(resultmsg);
                }

            }
        }
    }

    private void showResultDialogFail(final String result) {
        if (this!= null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    }

