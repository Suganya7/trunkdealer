package com.goserwizz.dealer.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.dealer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.Connectivity;
import com.goserwizz.dealer.app.Util.SettingsUtils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Kaptas on 3/15/2016.
 */
public class Profile extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    TextView dealeremail, dealername, dealermobile;

    private ProgressDialog pDialog;
    // URL to get contacts JSON
    private static String url;
    // JSON Node names
    private static final String TAG_FNAME = "fname";
    private static final String TAG_LNAME = "lname";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_MOBILE = "mob";
    private static final String TAG_GDID = "gd_id";
    private static final String TAG_MODE = "mode";

    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    String status, resultmsg;
    String fname,pwd, gd_id;
    String gu_id, mobile;
    TextView changePassword;
    LinearLayout ProfileLayout;
    Button ProfileSaveBtn;
    ImageView editName, editMobile;
    String pid, lname, compname, vmake, vtype, dtype, stype, mob, email, logo, vmodel;
    String location, state, addr, longi, lat, country, city, amenity, name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        SettingsUtils.init(this);
        url = SettingsUtils.API_URL_DELARDETAIL;

        gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");

        toolbar = (Toolbar)findViewById(R.id.Profiletoolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");

        ProfileLayout = (LinearLayout)findViewById(R.id.ProfileLayout);
        setupUI(ProfileLayout);

        dealeremail =(TextView)findViewById(R.id.dealerEmail);
        dealername = (EditText)findViewById(R.id.dealerName);
      //  profile_pwd = (EditText)findViewById(R.id.profile_pwdd);
        dealermobile = (EditText)findViewById(R.id.dealermobileNum);
        editName = (ImageView)findViewById(R.id.edit_name);
        editMobile = (ImageView)findViewById(R.id.edit_mobile);
        ProfileSaveBtn = (Button)findViewById(R.id.profileUpdate);
        ProfileSaveBtn.setOnClickListener(this);
        editName.setOnClickListener(this);
        editMobile.setOnClickListener(this);


        if(Connectivity.isConnected(this)) {

            new JsonGetUserDetails().execute();
        }
        else {
            Connectivity.showNoConnectionDialog(this);
        }

        TextView changePassword = (TextView)findViewById(R.id.changePassword);
        changePassword.setPaintFlags(changePassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        changePassword.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                startActivity(new Intent(Profile.this, ChangePassword.class));
            }
        });




    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {

            case R.id.profileUpdate:

                name = dealername.getText().toString();
                mobile = dealermobile.getText().toString();
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, name);
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mobile);

                if(Connectivity.isConnected(this)) {

                    new JsonUpdateUserDetails().execute();
                }
                else {
                    Connectivity.showNoConnectionDialog(this);
                }

                break;

            case R.id.edit_name:
                dealername.setEnabled(true);
                break;

            case R.id.edit_mobile:
                dealermobile.setEnabled(true);
                break;
        }
    }



    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonUpdateUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            String pid = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PID, "");

            if(pid.equals("0")) {
                fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
                lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");

            }else {
                fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
                lname = "null";

            }

            email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");
            mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_FNAME, fname));
            nameValuePairs.add(new BasicNameValuePair(TAG_LNAME, lname));
            nameValuePairs.add(new BasicNameValuePair(TAG_MOBILE, mob));
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, email));
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "updateDet"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);

                            }
                        }

                        System.out.println(resultmsg);

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(Profile.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonUpdateUserDetails().execute();


                    }
                    else {
                        showResultDialog(resultmsg);
                    }
                }
                else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonUpdateUserDetails().execute();


                    } else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialog(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }



    private void showResultDialog(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);

                            new JsonGetUserDetailsUpdate().execute();
                            //   finish();

                        }
                    })
                            //.setNegativeButton("Cancel", null)
                    .show();
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetUserDetails extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getDetails"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    pid = tmpObj.get("pid").toString();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PID, pid);

                                    if(pid.equals("0")) {

                                        pid = tmpObj.get("pid").toString();
                                        gd_id = tmpObj.get("gd_id").toString();
                                        fname = tmpObj.get("fname").toString();
                                        lname = tmpObj.get("lname").toString();
                                        compname = tmpObj.get("compname").toString();
                                        vmake = tmpObj.get("vmake").toString();
                                        vtype = tmpObj.get("vtype").toString();
                                        dtype = tmpObj.get("dtype").toString();
                                        stype = tmpObj.get("stype").toString();
                                        mob = tmpObj.get("mob").toString();
                                        email = tmpObj.get("email").toString();
                                        logo = tmpObj.get("logo").toString();
                                        vmodel = tmpObj.get("vmodel").toString();

                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);


                                    }
                                    else {

                                        pid = tmpObj.get("pid").toString();
                                        gd_id = tmpObj.get("gd_id").toString();
                                        name = tmpObj.get("name").toString();
                                        compname = tmpObj.get("compname").toString();
                                        vmake = tmpObj.get("vmake").toString();
                                        vtype = tmpObj.get("vtype").toString();
                                        dtype = tmpObj.get("dtype").toString();
                                        stype = tmpObj.get("stype").toString();
                                        mob = tmpObj.get("mob").toString();
                                        email = tmpObj.get("email").toString();
                                        vmodel = tmpObj.get("vmodel").toString();
                                        location = tmpObj.get("location").toString();
                                        state = tmpObj.get("state").toString();
                                        addr = tmpObj.get("addr").toString();
                                        longi = tmpObj.get("long").toString();
                                        lat = tmpObj.get("lat").toString();
                                        country = tmpObj.get("country").toString();
                                        city = tmpObj.get("city").toString();
                                        amenity = tmpObj.get("amenity").toString();

                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, name);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);

                                    }

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GD_ID, gd_id);

                                    Log.d("PID::", pid);
                                }

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(Profile.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetails().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {

                        fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
                        lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");
                        pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");
                        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
                        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

                        dealeremail.setText(email);
                        dealername.setText(fname);
                      //  profile_pwd.setText(pwd);
                        dealermobile.setText(mob);
                    }
                } else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetails().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JsonGetUserDetailsUpdate extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Profile.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getDetails"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while(jObj_Keys.hasNext() ) {
                            String jObj_key = (String)jObj_Keys.next();

                            if(jObj_key.equals("data")){
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    pid = tmpObj.get("pid").toString();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PID, pid);

                                    if(pid.equals("0")) {

                                        pid = tmpObj.get("pid").toString();
                                        gd_id = tmpObj.get("gd_id").toString();
                                        fname = tmpObj.get("fname").toString();
                                        lname = tmpObj.get("lname").toString();
                                        compname = tmpObj.get("compname").toString();
                                        vmake = tmpObj.get("vmake").toString();
                                        vtype = tmpObj.get("vtype").toString();
                                        dtype = tmpObj.get("dtype").toString();
                                        stype = tmpObj.get("stype").toString();
                                        mob = tmpObj.get("mob").toString();
                                        email = tmpObj.get("email").toString();
                                        logo = tmpObj.get("logo").toString();
                                        vmodel = tmpObj.get("vmodel").toString();

                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);


                                    }
                                    else {

                                        pid = tmpObj.get("pid").toString();
                                        gd_id = tmpObj.get("gd_id").toString();
                                        name = tmpObj.get("name").toString();
                                        compname = tmpObj.get("compname").toString();
                                        vmake = tmpObj.get("vmake").toString();
                                        vtype = tmpObj.get("vtype").toString();
                                        dtype = tmpObj.get("dtype").toString();
                                        stype = tmpObj.get("stype").toString();
                                        mob = tmpObj.get("mob").toString();
                                        email = tmpObj.get("email").toString();
                                        vmodel = tmpObj.get("vmodel").toString();
                                        location = tmpObj.get("location").toString();
                                        state = tmpObj.get("state").toString();
                                        addr = tmpObj.get("addr").toString();
                                        longi = tmpObj.get("long").toString();
                                        lat = tmpObj.get("lat").toString();
                                        country = tmpObj.get("country").toString();
                                        city = tmpObj.get("city").toString();
                                        amenity = tmpObj.get("amenity").toString();

                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, name);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);

                                    }

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GD_ID, gd_id);

                                    Log.d("PID::", pid);
                                }

                            }
                        }

                    }
                    else if(status.equals("fail")) {
                        resultmsg = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing()) {
                pDialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */

            String Connection = (Connectivity.isConnected(Profile.this) ? getString(R.string.dialog_pass) : getString(R.string.dialog_fail));

            if(Connection.equals("Passed")) {

                if(result.equals("success")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetailsUpdate().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else {

                        fname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
                        lname = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_LASTNAME, "");
                        pwd = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_PWD, "");
                        mob = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_MOBILE, "");
                        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

                        dealeremail.setText(email);
                        dealername.setText(fname);
                     //   profile_pwd.setText(pwd);
                        dealermobile.setText(mob);

                        //   finish();
                    }
                } else if(result.equals("fail")) {

                    if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet successfully")) {

                        new JsonGetUserDetailsUpdate().execute();
                        //  showResultDialogFail(getString(R.string.dialog_system_connection_test) + "Failed, Please try again" );
                    }
                    else { showResultDialogFail(resultmsg); }
                }

            }
            else {
                showResultDialogFail(getString(R.string.dialog_system_connection_test) + Connection + ", Please try again");
            }

        }
    }


    private void showResultDialog1(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


    private void showResultDialogFail(final String result) {
        if (this != null && !this.isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(this)
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(Profile.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


}
