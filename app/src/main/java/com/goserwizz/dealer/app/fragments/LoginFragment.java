package com.goserwizz.dealer.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goserwizz.dealer.app.JsonWebservice.ServiceHandler;
import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.Connectivity;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.activities.ForgetPassword;
import com.goserwizz.dealer.app.activities.HomeActivity;
import com.goserwizz.dealer.app.activities.PrivacyPolicy;
import com.goserwizz.dealer.app.activities.TermsCondition;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class LoginFragment extends Fragment {

    EditText emailid, password;
    LinearLayout signin;
    public LoginFragment() {
        // Required empty public constructor
    }

    TextView loginfacebook;
    private ProgressDialog pDialog, Dialog;
    // URL to get contacts JSON  3vikram.in
    private static String url;
    // JSON Node names
    private static final String TAG_MODE = "mode";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PWD = "pwd";
    private static final String TAG_GDID = "gd_id";
    private static final String TAG_SESSION = "session";
    private static final String TAG_FNAME = "fname";
    private static final String TAG_MSG = "msg";
    private static final String TAG_STATUS = "status";
    private static final String TAG_DIGEST = "digest";

    // Hashmap for ListView
    ArrayList<HashMap<String, String>> userList;
    String Email, Pwd, gd_id, session, digest, fname, status, resultmsg, resultmsg1;

    TextView forgetPwd;

    private static String url2;
    // JSON Node names
    String pid, lname, compname, vmake, vtype, dtype, stype, mob, email, logo, vmodel;
    String location, state, addr, longi, lat, country, city, amenity, name;
    FrameLayout loginlayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        SettingsUtils.init(getActivity());
        url = SettingsUtils.API_URL_LOGIN;
        url2 = SettingsUtils.API_URL_DELARDETAIL;

        emailid = (EditText) view.findViewById(R.id.login_email);
        password = (EditText) view.findViewById(R.id.login_pwd);
        forgetPwd = (TextView) view.findViewById(R.id.forgetPwd);
        signin = (LinearLayout) view.findViewById(R.id.SignInLayout);
        loginlayout = (FrameLayout) view.findViewById(R.id.loginlayout);
        password.setTypeface(Typeface.DEFAULT);

        setupUI(loginlayout);

        emailid.addTextChangedListener(new MyTextWatcher(emailid));
        password.addTextChangedListener(new MyTextWatcher(password));

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        forgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Email = emailid.getText().toString();
                SettingsUtils.getInstance().putValue(SettingsUtils.KEY_USER_EMAIL, Email);

                startActivity(new Intent(getActivity(), ForgetPassword.class));

                emailid.setText("");



            }
        });


        /* Spannable functions for underline text which clicks to open another activity  */
        TextView terms = (TextView) view.findViewById(R.id.agreeText1);
        TextView condition = (TextView) view.findViewById(R.id.agreeText2);
        TextView privacypolicy = (TextView) view.findViewById(R.id.agreeText4);

        terms.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans = (Spannable) terms.getText();
        ClickableSpan clickSpan = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
               startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans.setSpan(clickSpan, 0, spans.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//
        condition.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans1 = (Spannable) condition.getText();
        ClickableSpan clickSpan1 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), TermsCondition.class));
            }
        };
        spans1.setSpan(clickSpan1, 0, spans1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
        privacypolicy.setMovementMethod(LinkMovementMethod.getInstance());

        Spannable spans2 = (Spannable) privacypolicy.getText();
        ClickableSpan clickSpan2 = new ClickableSpan() {

            @Override
            public void onClick(View widget)
            {
                startActivity(new Intent(getActivity(), PrivacyPolicy.class));
            }
        };
        spans2.setSpan(clickSpan2, 0, spans2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

 /* Spannable functions for underline text functions finist */

        return view;
    }


    /**
     * Validating form
     */
    private void submitForm() {

        if (!validateEmail()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        Email = emailid.getText().toString();
        Pwd = password.getText().toString();
        if (Connectivity.isConnected(getActivity())) {

            new JSONGetLoginData().execute();
        } else {
            Connectivity.showNoConnectionDialog(getActivity());
        }

    }

    private boolean validateEmail() {
        String emailreg = emailid.getText().toString().trim();

        if (emailreg.isEmpty() || !isValidEmail(emailreg)) {
            emailid.setError(getString(R.string.err_msg_email));
            requestFocus(emailid);
            return false;
        } else {
            //email.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty()) {
            password.setError(getString(R.string.err_msg_password));
            requestFocus(password);
            return false;
        } else {
            //  password.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

                case R.id.login_email:
                    validateEmail();
                    break;
                case R.id.login_pwd:
                    validatePassword();
                    break;
            }
        }
    }

    /**
     * Async task class to get json by making HTTP call
     * */
    private class JSONGetLoginData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Logging in...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_EMAIL, Email));
            nameValuePairs.add(new BasicNameValuePair(TAG_PWD, Pwd));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "signin"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    status = jsonObj.getString(TAG_STATUS);
                    if(status.equals("success")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                        JSONObject object = jsonObj.getJSONObject("data");
                        gd_id = object.getString(TAG_GDID);
                        session = object.getString(TAG_SESSION);

                      //  fname = object.getString(TAG_FNAME);

                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GD_ID, gd_id);
                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGINSESSION, session);
                    }
                    else if(status.equals("fail")) {
                        resultmsg = jsonObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {

                    new JSONGetLoginData().execute();
                }
                else {


                    new JSONGetDealerData().execute();

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, true);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MODE, 1);

                  /*  SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, true);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MODE, 1);

                    Toast.makeText(getActivity(), resultmsg, Toast.LENGTH_LONG).show();

                    Intent i = new Intent(getActivity(), HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);  */
                }

            }
            else if(result.equals("fail")) {

                if(resultmsg.contains("Communications link failure") || resultmsg.contains("The last packet")) {

                    new JSONGetLoginData().execute();
                }
                else {
                    showResultDialogFail(resultmsg);
                }

            }
        }
    }



    public void onBackPressed() {

        Log.d("Login Fragment ===>", "user pressed the back button");

        getActivity().moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);


    }

    private void showResultDialogFail(final String result) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new android.support.v7.app.AlertDialog.Builder(getActivity())
                    .setMessage(result)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //  listener.onConnectionChecked(result);


                        }
                    })
                    .setCancelable(false)
                    .show();
        }
    }


    /**
     * Async task class to get json by making HTTP call
     * */
    private class JSONGetDealerData extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            Dialog = new ProgressDialog(getActivity());
            Dialog.setMessage("please wait...");
            Dialog.setCancelable(false);
            Dialog.show();
        }
        @Override
        protected String doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            gd_id = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_GD_ID, "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(TAG_GDID, gd_id));
            nameValuePairs.add(new BasicNameValuePair(TAG_MODE, "getDetails"));
            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url2, ServiceHandler.POST, nameValuePairs);
            Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {

                    JSONObject jObj = new JSONObject(jsonStr.trim());
                    Iterator<?> jObj_Keys = jObj.keys();

                    status = jObj.getString(TAG_STATUS);
                    resultmsg1 = jObj.getString(TAG_MSG);


                    if(status.equals("success")) {

                        while (jObj_Keys.hasNext()) {
                            String jObj_key = (String) jObj_Keys.next();

                            if (jObj_key.equals("data")) {
                                JSONObject dataObj = (JSONObject) jObj.get(jObj_key);
                                Iterator<?> dataObj_keys = dataObj.keys();

                                while (dataObj_keys.hasNext()) {
                                    String dataObj_key = (String) dataObj_keys.next();

                                    JSONObject tmpObj = (JSONObject) dataObj.get(dataObj_key);
                                    pid = tmpObj.get("pid").toString();

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_PID, pid);

                                    if(pid.equals("0")) {

                                        pid = tmpObj.get("pid").toString();
                                        gd_id = tmpObj.get("gd_id").toString();
                                        fname = tmpObj.get("fname").toString();
                                        lname = tmpObj.get("lname").toString();
                                        compname = tmpObj.get("compname").toString();
                                        vmake = tmpObj.get("vmake").toString();
                                        vtype = tmpObj.get("vtype").toString();
                                        dtype = tmpObj.get("dtype").toString();
                                        stype = tmpObj.get("stype").toString();
                                        mob = tmpObj.get("mob").toString();
                                        email = tmpObj.get("email").toString();
                                        logo = tmpObj.get("logo").toString();
                                        vmodel = tmpObj.get("vmodel").toString();

                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, fname);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, lname);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMAKE, vmake);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, vtype);


                                    }
                                    else {

                                        pid = tmpObj.get("pid").toString();
                                        gd_id = tmpObj.get("gd_id").toString();
                                        name = tmpObj.get("name").toString();
                                        compname = tmpObj.get("compname").toString();
                                        vmake = tmpObj.get("vmake").toString();
                                        vtype = tmpObj.get("vtype").toString();
                                        dtype = tmpObj.get("dtype").toString();
                                        stype = tmpObj.get("stype").toString();
                                        mob = tmpObj.get("mob").toString();
                                        email = tmpObj.get("email").toString();
                                        vmodel = tmpObj.get("vmodel").toString();
                                        location = tmpObj.get("location").toString();
                                        state = tmpObj.get("state").toString();
                                        addr = tmpObj.get("addr").toString();
                                        longi = tmpObj.get("long").toString();
                                        lat = tmpObj.get("lat").toString();
                                        country = tmpObj.get("country").toString();
                                        city = tmpObj.get("city").toString();
                                        amenity = tmpObj.get("amenity").toString();

                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FIRSTNAME, name);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_EMAIL, email);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MOBILE, mob);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LASTNAME, "");
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VMAKE, vmake);
                                        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_VTYPE, vtype);

                                    }

                                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_GD_ID, gd_id);

                                    Log.d("PID::", pid);
                                }
                            }
                        }
                    }
                    else if(status.equals("fail")) {
                        resultmsg1 = jObj.getString(TAG_MSG);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return status;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (Dialog.isShowing()) {
                Dialog.dismiss(); }
            /**
             * Updating parsed JSON data into ListView
             * */
            if(result.equals("success")) {

                if(resultmsg1.contains("Communications link failure") || result.contains("The last packet")) {

                    new JSONGetDealerData().execute();
                }
                else {

                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_LOGGED_IN, true);
                    SettingsUtils.getInstance().putValue(SettingsUtils.KEY_MODE, 1);

                    Toast.makeText(getActivity(), resultmsg, Toast.LENGTH_LONG).show();

                    Intent i = new Intent(getActivity(), HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }

            }
            else if(result.equals("fail")) {

                if(resultmsg.contains("Communications link failure") || result.contains("The last packet")) {

                    new JSONGetDealerData().execute();
                }
                else {
                    showResultDialogFail(resultmsg);
                }

            }
        }
    }

    public void setupUI(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


}
