package com.goserwizz.dealer.app.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goserwizz.dealer.app.R;
import com.goserwizz.dealer.app.Util.SettingsUtils;
import com.goserwizz.dealer.app.fragments.BookingDetails;
import com.goserwizz.dealer.app.fragments.HomeFragment;
import com.goserwizz.dealer.app.fragments.SignOut;

import java.io.IOException;

/**
 * Created by Kaptas on 3/30/2016.
 */
public class HomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    ImageView profileImg;
    TextView ProfileName, ProfileEmail;
    private int PICK_IMAGE_REQUEST = 1;

    LinearLayout mDrawerContentLayout;
    DrawerLayout drawer;
    View previousParentView = null, previousSubParentView = null;
    ImageView previousImgLogo = null, previousSubImgLogo = null;
    TextView previousTxtTitle = null, previousSubTxtTitle = null;
    int previouskey, parentPreviousKey;
    String name, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SettingsUtils.init(this);
        SettingsUtils.getInstance().putValue(SettingsUtils.KEY_FILTER, "");

        name = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_FIRSTNAME, "");
        email = SettingsUtils.getInstance().getValue(SettingsUtils.KEY_EMAIL, "");

        ProfileName = (TextView)findViewById(R.id.ProfileName);
        ProfileEmail = (TextView)findViewById(R.id.ProfileEmail);
        ProfileName.setText(name);
        ProfileEmail.setText(email);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerContentLayout = (LinearLayout) findViewById(R.id.navigation_content);

        initDrawerContent();

        LinearLayout headerLinearlayout = (LinearLayout) findViewById(R.id.headerlayout);
        headerLinearlayout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, Profile.class));

                closeNavigation();

            }

        });

        ImageView editlayout = (ImageView) findViewById(R.id.editProfile);
        editlayout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

              startActivity(new Intent(HomeActivity.this, Profile.class));

                closeNavigation();

            }

        });

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


       /* Intent i = new Intent(HomeActivity.this, HomeActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("isExitAction", true);
        startActivity(i);

       /* moveTaskToBack(false);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

        Intent newIntent = new Intent(this, HomeActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(newIntent);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

     //   menu.findItem(R.id.notification).setIcon(resizeImage(R.drawable.notification, 70, 70));
        menu.findItem(R.id.filters).setIcon(resizeImage(R.drawable.filters, 70, 70));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.filters) {

            startActivity(new Intent(HomeActivity.this, FilterActivity.class));

            return true;
        }

            return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean MyStartActivity(Intent aIntent) {
        try {
            startActivity(aIntent);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                profileImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void initDrawerContent() {
        mDrawerContentLayout.removeAllViews();
        for (int i = 0; i < 6; i++) {
            switch (i) {
             /*   case 6:
                    View naviMenu = getLayoutInflater().inflate(R.layout.row_navigation_menu_logo, null, false);
                    mDrawerContentLayout.addView(naviMenu);
                    break; */

                default:
                    View collapsableMenuView = getLayoutInflater().inflate(R.layout.row_drawer_item_normal, null, false);
                    final TextView txtMenuTitle = (TextView) collapsableMenuView.findViewById(R.id.textView2);
                    final ImageView imgMenuLogo = (ImageView) collapsableMenuView.findViewById(R.id.imageView_logo);
                    final View menuV = collapsableMenuView;
                    final int mkey = i;
                    switch (i) {
                        case 0:
                            txtMenuTitle.setText("Home");
                            imgMenuLogo.setImageResource(R.drawable.home);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.home_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Home");
                                    HomeFragment fragment = new HomeFragment();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    closeNavigation();

                                }
                            });
                            collapsableMenuView.performClick();
                            break;

                        case 1:
                            txtMenuTitle.setText("Booking Details");
                            imgMenuLogo.setImageResource(R.drawable.booking_details);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.booking_details_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Booking Details");
                                    BookingDetails fragment = new BookingDetails();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();
                                    closeNavigation();

                                }
                            });
                            break;
                        case 2:
                            txtMenuTitle.setText("Feedback & Support");
                            imgMenuLogo.setImageResource(R.drawable.feedback);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.feedback_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);

                                    startActivity(new Intent(HomeActivity.this, com.goserwizz.dealer.app.activities.SupportActivity.class));

                                    closeNavigation();

                                }
                            });

                            break;
                        case 3:
                            txtMenuTitle.setText("Terms & Condition");
                            imgMenuLogo.setImageResource(R.drawable.terms_condition);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.terms_condition_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);

                                    startActivity(new Intent(HomeActivity.this, TermsCondition.class));

                                    closeNavigation();

                                }
                            });

                            break;
                        case 4:
                            txtMenuTitle.setText("Privacy Policy");
                            imgMenuLogo.setImageResource(R.drawable.privacy);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.privacy_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);

                                    startActivity(new Intent(HomeActivity.this, PrivacyPolicy.class));

                                    closeNavigation();

                                }
                            });
                            break;
                        case 5:
                            txtMenuTitle.setText("Logout");
                            imgMenuLogo.setImageResource(R.drawable.logout);
                            collapsableMenuView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    imgMenuLogo.setImageResource(R.drawable.logout_selected);
                                    setSelectedView(menuV, imgMenuLogo, txtMenuTitle, mkey);
                                    getSupportActionBar().setTitle("Logout");
                                    SignOut fragment = new SignOut();
                                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                    fragmentTransaction.replace(R.id.frame, fragment);
                                    fragmentTransaction.commit();

                                    closeNavigation();

                                }
                            });
                            break;

                    }
                    mDrawerContentLayout.addView(collapsableMenuView);
                    break;

            }
        }


    }

    public void closeNavigation() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    public void setSelectedView(View parent, ImageView imgLogo, TextView txtTitle, int key) {
        parent.setBackgroundResource(R.drawable.shape_list_selected);
        txtTitle.setTextColor(Color.parseColor("#c42525"));
        if (parent != previousParentView)
            setNormalMainMenu();

        previousImgLogo = imgLogo;
        previousTxtTitle = txtTitle;
        previousParentView = parent;
        parentPreviousKey = key;
    }

    public void setNormalMainMenu() {
        switch (parentPreviousKey) {

            case 0:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.home);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 1:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.booking_details);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 2:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                        previousImgLogo.setImageResource(R.drawable.feedback);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 3:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.settings);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 4:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.call_support);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;
            case 5:

                if (previousParentView != null) {
                    previousParentView.setBackgroundResource(R.drawable.shape_list_normal);
                    previousImgLogo.setImageResource(R.drawable.logout);
                    previousTxtTitle.setTextColor(Color.parseColor("#616161"));
                }
                break;

        }

    }

    private Drawable resizeImage(int resId, int w, int h)
    {
        // load the origial Bitmap
        Bitmap BitmapOrg = BitmapFactory.decodeResource(getResources(), resId);
        int width = BitmapOrg.getWidth();
        int height = BitmapOrg.getHeight();
        int newWidth = w;
        int newHeight = h;
        // calculate the scale
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(BitmapOrg, 0, 0,width, height, matrix, true);
        return new BitmapDrawable(resizedBitmap);
    }


    // To close the app or exit use this to add in Onresume function//

    @Override
    protected void onResume() {
        super.onResume();
        try {
            Intent i = getIntent();
            Boolean isExit = i.getBooleanExtra("isExitAction",false);
            if(isExit){
                this.finish();
            }
        }
        catch (Exception e){}
    }




}
